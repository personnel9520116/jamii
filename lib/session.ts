import { getServerSession } from "next-auth/next";
import { authOptions } from "@/lib/auth-option";

export async function getSession() {
  return await getServerSession(authOptions);
}

export async function getCurrentUser() {
  const session = await getSession();

  if (!session || !session.user) {
    return null;
  }

  return {
    // id: session.user.id as string,
    name: session.user.name,
    email: session.user.email,
    image: session.user.image,
  };
}
