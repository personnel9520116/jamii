import { format, parseISO, parse } from "date-fns";
import { fr, enUS } from "date-fns/locale";
export const convertDate = (
  dateInput: Date | string,
  locale: string,
  separator: "/" | "-"
): string => {
  // Si la date est une chaîne de caractères, essayez de la parser
  let date: Date;
  if (typeof dateInput === "string") {
    if (dateInput.includes("T")) {
      // Si la date contient "T", alors c'est un format ISO complet
      date = parseISO(dateInput);
    } else {
      // Sinon, essayez de parser la date sans la partie temps
      date = parse(dateInput, "yyyy-MM-dd", new Date());
    }
  } else {
    date = dateInput;
  }
  let dateFormat: string;
  switch (locale) {
    case "fr":
      dateFormat = `dd${separator}MM${separator}yyyy`;
      break;
    case "en":
      dateFormat = `MM${separator}dd${separator}yy`;
      break;
    default:
      throw new Error("Unsupported locale");
  }
  return format(date, dateFormat, {
    locale: locale === "fr" ? fr : enUS,
  });
};
