"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import Input from "@/components/myComponent/Input ";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React from "react";
import { FieldValues, useForm } from "react-hook-form";

const ForgetPassword = () => {
  const data = [
    {
      id: "mail",
      placeholder: "Votre mail",
      type: "email",
      image: "/email.svg",
    },
  ];
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      nom: "",
      mail: "",
    },
  });
  const router = useRouter();
  return (
    <div className=" bg-[#000A21] w-full h-full lg:h-screen pt-[101px] pb-[26px] lg:py-[114px] flex flex-col lg:flex-row items-center justify-between gap-[54px] lg:px-[110px]">
      <div className="w-[50%] flex flex-col text-grisf justify-center gap-[72px] items-center">
        <Image
          src="/logo 2.svg"
          alt=""
          width={310}
          height={288}
          className=" h-[168px] w-[154px] lg:w-[310px] lg:h-[288px]"
        />
        <div className=" flex-col items-center justify-center gap-[22px] hidden lg:flex">
          <p className="">Je me souviens de mes coordonnées</p>
          <CustomButton
            onClick={() => router.push("/sign_in")}
            text="Se connecter"
            color="text-grisf"
            className=" !bg-[#FBFEFF1A]"
          />
        </div>
      </div>
      <div className="bg-[#00303F33] shadow-[0px_1px_12px_0px_#00000040] rounded-[25px] text-[#F0FAFC] w-full lg:w-[50%] flex items-center justify-center pt-[80px] pb-[53px] lg:pt-[90px] lg:pb-[195px]">
        <div className="flex items-center justify-center flex-col px-4 lg:px-[94px] w-full ">
          <div className="relative flex items-center justify-center ">
            <div className="  rounded-full h-[86px] w-[86px] bg-[#00303F] opacity-50 flex items-center justify-center lg:h-[123px] lg:w-[123px] "></div>
            <Image
              src="/Mail_ico.svg"
              alt=""
              width={44}
              height={44}
              className=" absolute"
            />
          </div>
          <p className=" text-lg text-center pt-[30px]">
            Veuillez renseigner le mail avec lequel votre compte a été crée.
          </p>
          <div className=" w-full">
            {data.map((input, index) => (
              <div className=" relative" key={index}>
                <Input
                  register={register}
                  errors={errors}
                  required
                  id={input.id}
                  placeholder={input.placeholder}
                  type={input.type}
                  design={"mt-[25px]"}
                  // name={input.name}
                  className={" rounded bg-transparent"}
                />
                {input.image && (
                  <img
                    src={`${input.image}`}
                    draggable="false"
                    alt=""
                    className=" absolute bottom-3 left-6 "
                  />
                )}
              </div>
            ))}
          </div>
          <div className="mt-[30px] lg:mt-11 w-full flex flex-col gap-[30px] items-center justify-center ">
            <CustomButton
              onClick={() => router.push("/reset_password")}
              text="Envoyer"
            />
          </div>
        </div>
      </div>
      <div className=" flex flex-col items-center justify-center lg:hidden text-grisp gap-4">
        <p className="">Je me souviens de mes coordonnées</p>
        <CustomButton
          onClick={() => router.push("/sign_in")}
          text="Se connecter"
          color="text-grisf"
          className=" !bg-[#FBFEFF1A]"
        />
      </div>
    </div>
  );
};

export default ForgetPassword;
