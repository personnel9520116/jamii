"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import Input from "@/components/myComponent/Input ";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";

const ResetPassword = () => {
  const data = [
    {
      id: "nom",
      placeholder: "Votre nom complet",
      type: "text",
      image: "/user.svg",
    },
    {
      id: "mail",
      placeholder: "Votre mail",
      type: "email",
      image: "/email.svg",
    },
  ];
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      nom: "",
      mail: "",
    },
  });
  const onSubmit: SubmitHandler<FieldValues> = (values) => {};
  const router = useRouter();
  return (
    <div className=" bg-[#000A21] w-full min-h-screen pt-[101px] pb-[26px] lg:py-[114px] flex flex-col lg:flex-row items-center justify-between gap-[54px] lg:px-[110px]">
      <div className="w-[50%] flex justify-center items-center">
        <Image
          src="/logo 2.svg"
          alt=""
          width={310}
          height={288}
          className=" h-[168px] w-[154px] lg:w-[310px] lg:h-[288px]"
        />
      </div>
      <div className="bg-[#00303F33] shadow-[0px_1px_12px_0px_#00000040] rounded-[25px] text-[#F0FAFC] w-full lg:w-[50%] flex items-center justify-center pt-[80px] pb-[53px] lg:pt-[90px] lg:pb-[195px]">
        <div className="flex items-center justify-center flex-col px-4 lg:px-[94px] w-full ">
          <div className="relative flex items-center justify-center ">
            <div className="  rounded-full h-[86px] w-[86px] bg-[#FF7C03] opacity-10 flex items-center justify-center lg:h-[123px] lg:w-[123px] "></div>
            <Image
              src="/resetIcon.svg"
              alt=""
              width={44}
              height={44}
              className=" absolute"
            />
          </div>
          <p className=" text-lg text-center pt-[30px]">
            Définissez votre nouveau mot de passe
          </p>
          <div className=" w-full">
            {data.map((input, index) => (
              <div className=" relative" key={index}>
                <Input
                  placeholder={input.placeholder}
                  type={input.type}
                  design={"mt-[25px]"}
                  register={register}
                  errors={errors}
                  required
                  id={input.id}
                  className={" rounded bg-transparent"}
                />
                {input.image && (
                  <img
                    src={`${input.image}`}
                    draggable="false"
                    alt=""
                    className=" absolute bottom-3 left-6 "
                  />
                )}
              </div>
            ))}
          </div>
          <div className="mt-[30px] lg:mt-[92px] w-full flex flex-col gap-[30px] items-center justify-center ">
            <CustomButton
              onClick={handleSubmit(onSubmit)}
              text="Réinitialiser"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResetPassword;
