"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import Input from "@/components/myComponent/Input ";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, { useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { signIn } from "next-auth/react";
import { toast } from "react-toastify";

const Connexion = () => {
  const data = [
    {
      id: "mail",
      placeholder: "Votre mail",
      type: "email",
      image: "/email.svg",
    },
    {
      id: "password",
      placeholder: "Mot de passe",
      type: "password",
      image: "/user.svg",
    },
  ];
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      mail: "",
      password: "",
    },
  });
  const [disabled, setDisabled] = useState();
  const onSubmit: SubmitHandler<FieldValues> = (values) => {
    signIn("credentials", {
      email: values.mail,
      password: values.password,
      redirect: false,
    })
      .then((callback) => {
        if (callback?.ok) {
          router.push("/admin");
          router.refresh();
          toast.success("Connexion réussie");
        }
        if (callback?.error) {
          toast.error(`${callback.error}`);
        }
      })
      .catch(() => toast.error("something went wrong"));
  };
  return (
    <div className=" bg-[#000A21] w-full h-full lg:py-[114px] flex flex-col min-h-screen items-center justify-center gap-[54px] lg:px-[110px]">
      <Image
        src="/logo 2.svg"
        alt=""
        width={230}
        height={210}
        className=" lg:hidden"
      />
      <div className="bg-[#00303F33] shadow-[0px_1px_12px_0px_#00000040] rounded-[25px] text-[#F0FAFC] w-full flex items-center justify-center pt-[80px] pb-[53px] lg:pt-[90px] lg:pb-[195px]">
        <div className="flex items-center justify-center flex-col px-4 w-full lg:max-w-[451px]">
          <Image
            src="/logo 2.svg"
            alt=""
            width={118}
            height={108}
            className="hidden lg:block"
          />
          <p className=" text-lg text-center pt-[30px]">
            Veuillez vous connecter avec votre compte administrateur 🙂
          </p>
          <div className=" w-full">
            {data.map((input, index) => (
              <div className=" relative" key={index}>
                <Input
                  placeholder={input.placeholder}
                  type={input.type}
                  design={"mt-[25px]"}
                  id={input.id}
                  className={" rounded bg-transparent"}
                  disabled={disabled}
                  register={register}
                  required
                  errors={errors}
                />
                {input.image && (
                  <img
                    src={`${input.image}`}
                    draggable="false"
                    alt=""
                    className=" absolute bottom-3 left-6 "
                  />
                )}
              </div>
            ))}
          </div>
          <div className="mt-[30px] lg:mt-11 w-full flex flex-col gap-[30px] items-center justify-center ">
            <CustomButton
              onClick={handleSubmit(onSubmit)}
              text="Se connecter"
            />
            <p
              onClick={() => router.push("/forget_password")}
              className=" cursor-pointer"
            >
              Mot de passe oublié ?
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Connexion;
