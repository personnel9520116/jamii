import { NextResponse } from "next/server";
import prisma from "@/lib/bd";
import { uploadFile } from "@/lib/cloudinary";

export async function GET(
  request: Request,
  { params }: { params: { id: string } }
) {
  try {
    const post = await prisma.post.findUnique({
      where: { id: params.id },
      include: {
        author: {
          select: {
            id: true,
            name: true,
            image: true,
          },
        },
      },
    });
    if (!post) {
      return NextResponse.json({ error: "Post not found" }, { status: 404 });
    }
    return NextResponse.json(post);
  } catch (error) {
    console.error("Error fetching post:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}

export async function PUT(
  request: Request,
  { params }: { params: { id: string } }
) {
  try {
    const body = await request.json();
    let imageUrl = body.image;
    // Vérifier si l'image est un base64
    if (body.image && body.image.startsWith("data:image")) {
      const result = await uploadFile(body.image);
      imageUrl = result.secure_url;
    } else {
      imageUrl = body.image;
    }
    const updatedPost = await prisma.post.update({
      where: { id: params.id },
      data: {
        title: body.title,
        description: body.description,
        category: body.category,
        image: imageUrl,
        share: body.share,
        content: body.content,
        metaTitle: body.metaTitle,
        metaDescription: body.metaDescription,
      },
    });
    return NextResponse.json(updatedPost);
  } catch (error) {
    console.error("Error updating post:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
export async function DELETE(
  request: Request,
  { params }: { params: { id: string } }
) {
  try {
    const article = await prisma.post.delete({
      where: {
        id: params.id,
      },
    });
    return NextResponse.json(article, { status: 201 });
  } catch (error) {
    console.log(error);
    return NextResponse.json("Failed to delete article", { status: 500 });
  }
}
