import prisma from "@/lib/bd";
import { NextRequest, NextResponse } from "next/server";
import { uploadFile } from "@/lib/cloudinary";
import { getCurrentUser } from "@/lib/session";

export async function GET() {
  try {
    const posts = await prisma.post.findMany({
      select: {
        id: true,
        title: true,
        description: true,
        image: true,
        share: true,
        author: {
          select: {
            id: true,
            name: true,
            image: true,
          },
        },
        createdAt: true,
        updatedAt: true,
        category: true,
      },
    });
    return NextResponse.json(posts);
  } catch (error) {
    console.error("Error fetching posts:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}

export async function POST(request: NextRequest) {
  const { data } = await request.json();
  const session = await getCurrentUser();

  try {
    const result = await uploadFile(data.image);
    const article = await prisma.post.create({
      data: {
        image: result.secure_url,
        share: data.share,
        content: data.content,
        slug: data.slug,
        title: data.title,
        description: data.description,
        metaDescription: data.metaDescription,
        metaTitle: data.metaTitle,
        author: {
          connect: {
            email: session?.email as string,
          },
        },
      },
    });
    return NextResponse.json(article, { status: 201 });
  } catch (error) {
    console.log(error);
    return NextResponse.json("Failed to create article", { status: 500 });
  }
}

export async function PATCH(request: Request, params: { articleId: string }) {
  const { articleId } = params;
  const { data } = await request.json();
  const session = await getCurrentUser();
  try {
    const article = await prisma.post.update({
      where: {
        id: articleId,
      },
      data: {
        content: data.content,
        slug: data.slug,
        title: data.title,
        description: data.description,
        metaDescription: data.metaDescription,
        metaTitle: data.metaTitle,
        author: {
          connect: {
            email: session?.email as string,
          },
        },
      },
    });
    return NextResponse.json(article, { status: 201 });
  } catch (error) {
    console.log(error);
    return NextResponse.json("Failed to update article", { status: 500 });
  }
}
