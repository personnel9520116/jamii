import { getCurrentUser } from "@/lib/session";
import { redirect } from "next/navigation";
import { ClientWrapper } from "@/components/ClientWrapper";

type Props = {
  children: React.ReactNode;
};

export default async function AdminController({ children }: Props) {
  const user = await getCurrentUser();

  if (!user) {
    redirect("/sign_in");
  }

  return <ClientWrapper user={user}>{children}</ClientWrapper>;
}
