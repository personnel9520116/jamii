import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Card } from "@/components/ui/card";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import prisma from "@/lib/bd";
import Image from "next/image";
import React from "react";
export default async function Component() {
  const globalData = [
    {
      image: "/Jamii President.png",
      title: "Gestion des déchets",
      createAt: "27 Février 2024",
      updateAt: "27 Février 2024",
      option: "/Option_ico.svg",
    },
    {
      image: "/Jamii President.png",
      title: "Gestion des déchets",
      createAt: "27 Février 2024",
      updateAt: "27 Février 2024",
      option: "/Option_ico.svg",
    },
    {
      image: "/Jamii President.png",
      title: "Gestion des déchets",
      createAt: "27 Février 2024",
      updateAt: "27 Février 2024",
      option: "/Option_ico.svg",
    },
    {
      image: "/Jamii President.png",
      title: "Gestion des déchets",
      createAt: "27 Février 2024",
      updateAt: "27 Février 2024",
      option: "/Option_ico.svg",
    },
  ];
  const optionData = [
    {
      image: "/edit_ico.svg",
      option: "Modifier le projet",
      color: "text-[#0C8B28]",
    },
    { image: "/Archive_ico.svg", option: "Archiver", color: "text-[#FF7C03]" },
    { image: "/trash_ico.svg", option: "Supprimer", color: "text-[#FF1422]" },
  ];
  return (
    <div className=" h-full flex flex-col relative gap-[30px] ">
      <Image
        src={"/Logo filigrane.svg"}
        className="absolute right-[-92px] top-[10px]"
        alt="logo"
        width={350}
        height={300}
      />
      <div className=" my-[60px] font-bold text-[32px]">
        <p>Bienvenue !</p>
        <p>BONY Roland 😃</p>
      </div>
      <div className=" flex gap-x-5">
        <div className=" text-grisf w-full relative bg-[#007194] border rounded-[10px] h-[196px] py-[15px] px-[30px]">
          <div className=" flex items-center gap-x-[18px]">
            <div className="h-[44px] w-[44px] rounded-full bg-[#00303F] flex items-center justify-center">
              <Image src={"/blog_ico.svg"} height={21} width={23} alt="" />
            </div>
            <p className=" text-lg font-bold">Notre blog</p>
          </div>
          <div className=" flex items-center justify-between mt-[30px] ">
            <div className=" w-full">
              <p className=" font-black text-[48px] ">123</p>
              <p className="text-lg">Articles</p>
            </div>
            <div className=" flex items-center gap-x-[42px] w-full">
              <hr className=" rounded-[10px] h-[82px] border border-[#5C7F87]" />
              <div>
                <p className=" font-black text-[48px] ">123</p>
                <p className="text-lg"> nouveau articles</p>
              </div>
            </div>
          </div>
          <div className=" absolute bottom-0 right-0">
            <Image src={"/Article.svg"} alt="" height={191} width={166} />
          </div>
        </div>
        <div className=" text-noir relative w-full bg-blanch border rounded-[10px] h-[196px] py-[15px] px-[30px]">
          <div className=" flex items-center gap-x-[18px]">
            <div className="h-[44px] w-[44px] rounded-full bg-[#FFE7C2] flex items-center justify-center">
              <Image src={"/Projet_ico.svg"} height={21} width={23} alt="" />
            </div>
            <p className=" text-lg font-bold">Nos projets</p>
          </div>
          <div className=" flex items-center justify-between mt-[30px] ">
            <div className=" w-full">
              <p className=" font-black text-[48px] ">123</p>
              <p className="text-lg">Projets</p>
            </div>
            <div className=" flex items-center gap-x-[42px] w-full text-gris">
              <hr className=" rounded-[10px] h-[82px] border border-gris" />
              <div>
                {/* <p className=" font-black text-[48px] ">123</p> */}
                <p className="text-lg">Pas de nouveau projets</p>
              </div>
            </div>
          </div>
          <div className=" absolute bottom-0 right-0">
            <Image src={"/Project.svg"} alt="" height={191} width={166} />
          </div>
        </div>
      </div>
      <Card className=" px-[30px] pt-6">
        <table className="w-full">
          <thead>
            <tr className=" grid grid-cols-4">
              <th className="p-4 text-left">Nom</th>
              <th className="p-4 text-center">
                <div className=" flex justify-center gap-x-[10px]">
                  <Image
                    src={"/Calendar_ico.svg"}
                    alt=""
                    height={16}
                    width={16}
                  />
                  <p>Date d’ajout</p>
                </div>
              </th>
              <th className="p-4 text-center">
                <div className=" flex w-full gap-x-[10px] justify-center ">
                  <Image
                    src={"/calendarlast.svg"}
                    alt=""
                    height={16}
                    width={16}
                  />
                  <p>Dernière modification</p>
                </div>
              </th>
              <th className="p-4 text-left"></th>
            </tr>
          </thead>
          <tbody className=" flex flex-col gap-[10px]">
            {globalData.map((data, index) => (
              <tr
                key={index}
                className="grid grid-cols-4 shadow-[0px_3px_5px_0px_#2F404A26] rounded-lg"
              >
                <td className="pl-[30px] py-3">
                  <div className=" flex gap-x-2  items-center">
                    <Avatar className={" h-[58px] w-[58px]"}>
                      <AvatarImage src={data.image} alt="test" />
                      <AvatarFallback>cn</AvatarFallback>
                    </Avatar>
                    <p className=" font-bold text-lg text-noir">{data.title}</p>
                  </div>
                </td>
                <td className="p-4 flex  justify-center">{data.createAt}</td>
                <td className="p-4 flex justify-center">{data.updateAt}</td>
                <td className="pr-[30px] py-3 flex items-center justify-end ">
                  <Popover>
                    <PopoverTrigger asChild>
                      <Image
                        src={data.option}
                        className=" cursor-pointer"
                        alt=""
                        height={37}
                        width={37}
                      />
                    </PopoverTrigger>
                    <PopoverContent
                      side="left"
                      align="start"
                      className="p-5 bg-grisf border-none flex gap-7 flex-col pl-6 pr-10 py-7 w-auto"
                    >
                      {optionData.map((optionData, index) => (
                        <div
                          className=" flex gap-2 items-center cursor-pointer"
                          key={index}
                        >
                          <Image
                            src={optionData.image}
                            alt="logo"
                            width={19}
                            height={21}
                          />
                          <p className={`${optionData.color}`}>
                            {optionData.option}
                          </p>
                        </div>
                      ))}
                    </PopoverContent>
                  </Popover>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Card>
    </div>
  );
}

// const Component = () => {
//   const globalData = [
//     {
//       image: "/Jamii President.png",
//       title: "Gestion des déchets",
//       createAt: "27 Février 2024",
//       updateAt: "27 Février 2024",
//       option: "/Option_ico.svg",
//     },
//     {
//       image: "/Jamii President.png",
//       title: "Gestion des déchets",
//       createAt: "27 Février 2024",
//       updateAt: "27 Février 2024",
//       option: "/Option_ico.svg",
//     },
//     {
//       image: "/Jamii President.png",
//       title: "Gestion des déchets",
//       createAt: "27 Février 2024",
//       updateAt: "27 Février 2024",
//       option: "/Option_ico.svg",
//     },
//     {
//       image: "/Jamii President.png",
//       title: "Gestion des déchets",
//       createAt: "27 Février 2024",
//       updateAt: "27 Février 2024",
//       option: "/Option_ico.svg",
//     },
//   ];
//   const optionData = [
//     {
//       image: "/edit_ico.svg",
//       option: "Modifier le projet",
//       color: "text-[#0C8B28]",
//     },
//     { image: "/Archive_ico.svg", option: "Archiver", color: "text-[#FF7C03]" },
//     { image: "/trash_ico.svg", option: "Supprimer", color: "text-[#FF1422]" },
//   ];
//   return (
//     <div className=" h-full flex flex-col relative gap-[30px] ">
//       <Image
//         src={"/Logo filigrane.svg"}
//         className="absolute right-[-92px] top-[10px]"
//         alt="logo"
//         width={350}
//         height={300}
//       />
//       <div className=" pb-[60px] font-bold text-[32px]">
//         <p>Bienvenue !</p>
//         <p>BONY Roland 😃</p>
//       </div>
//       <div className=" flex gap-x-5">
//         <div className=" text-grisf w-full relative bg-[#007194] border rounded-[10px] h-[196px] py-[15px] px-[30px]">
//           <div className=" flex items-center gap-x-[18px]">
//             <div className="h-[44px] w-[44px] rounded-full bg-[#00303F] flex items-center justify-center">
//               <Image src={"/blog_ico.svg"} height={21} width={23} alt="" />
//             </div>
//             <p className=" text-lg font-bold">Notre blog</p>
//           </div>
//           <div className=" flex items-center justify-between mt-[30px] ">
//             <div className=" w-full">
//               <p className=" font-black text-[48px] ">123</p>
//               <p className="text-lg">Articles</p>
//             </div>
//             <div className=" flex items-center gap-x-[42px] w-full">
//               <hr className=" rounded-[10px] h-[82px] border border-[#5C7F87]" />
//               <div>
//                 <p className=" font-black text-[48px] ">123</p>
//                 <p className="text-lg"> nouveau articles</p>
//               </div>
//             </div>
//           </div>
//           <div className=" absolute bottom-0 right-0">
//             <Image src={"/Article.svg"} alt="" height={191} width={166} />
//           </div>
//         </div>
//         <div className=" text-noir relative w-full bg-blanch border rounded-[10px] h-[196px] py-[15px] px-[30px]">
//           <div className=" flex items-center gap-x-[18px]">
//             <div className="h-[44px] w-[44px] rounded-full bg-[#FFE7C2] flex items-center justify-center">
//               <Image src={"/Projet_ico.svg"} height={21} width={23} alt="" />
//             </div>
//             <p className=" text-lg font-bold">Nos projets</p>
//           </div>
//           <div className=" flex items-center justify-between mt-[30px] ">
//             <div className=" w-full">
//               <p className=" font-black text-[48px] ">123</p>
//               <p className="text-lg">Projets</p>
//             </div>
//             <div className=" flex items-center gap-x-[42px] w-full text-gris">
//               <hr className=" rounded-[10px] h-[82px] border border-gris" />
//               <div>
//                 {/* <p className=" font-black text-[48px] ">123</p> */}
//                 <p className="text-lg">Pas de nouveau projets</p>
//               </div>
//             </div>
//           </div>
//           <div className=" absolute bottom-0 right-0">
//             <Image src={"/Project.svg"} alt="" height={191} width={166} />
//           </div>
//         </div>
//       </div>
//       <Card className=" px-[30px] pt-6">
//         <table className="w-full">
//           <thead>
//             <tr className=" grid grid-cols-4">
//               <th className="p-4 text-left">Nom</th>
//               <th className="p-4 text-center">
//                 <div className=" flex justify-center gap-x-[10px]">
//                   <Image
//                     src={"/Calendar_ico.svg"}
//                     alt=""
//                     height={16}
//                     width={16}
//                   />
//                   <p>Date d’ajout</p>
//                 </div>
//               </th>
//               <th className="p-4 text-center">
//                 <div className=" flex w-full gap-x-[10px] justify-center ">
//                   <Image
//                     src={"/calendarlast.svg"}
//                     alt=""
//                     height={16}
//                     width={16}
//                   />
//                   <p>Dernière modification</p>
//                 </div>
//               </th>
//               <th className="p-4 text-left"></th>
//             </tr>
//           </thead>
//           <tbody className=" flex flex-col gap-[10px]">
//             {globalData.map((data, index) => (
//               <tr
//                 key={index}
//                 className="grid grid-cols-4 shadow-[0px_3px_5px_0px_#2F404A26] rounded-lg"
//               >
//                 <td className="pl-[30px] py-3">
//                   <div className=" flex gap-x-2  items-center">
//                     <Avatar className={" h-[58px] w-[58px]"}>
//                       <AvatarImage src={data.image} alt="test" />
//                       <AvatarFallback>cn</AvatarFallback>
//                     </Avatar>
//                     <p className=" font-bold text-lg text-noir">{data.title}</p>
//                   </div>
//                 </td>
//                 <td className="p-4 flex  justify-center">{data.createAt}</td>
//                 <td className="p-4 flex justify-center">{data.updateAt}</td>
//                 <td className="pr-[30px] py-3 flex items-center justify-end ">
//                   <Popover>
//                     <PopoverTrigger asChild>
//                       <Image
//                         src={data.option}
//                         className=" cursor-pointer"
//                         alt=""
//                         height={37}
//                         width={37}
//                       />
//                     </PopoverTrigger>
//                     <PopoverContent
//                       side="left"
//                       align="start"
//                       className="p-5 bg-grisf border-none flex gap-7 flex-col pl-6 pr-10 py-7 w-auto"
//                     >
//                       {optionData.map((optionData, index) => (
//                         <div
//                           className=" flex gap-2 items-center cursor-pointer"
//                           key={index}
//                         >
//                           <Image
//                             src={optionData.image}
//                             alt="logo"
//                             width={19}
//                             height={21}
//                           />
//                           <p className={`${optionData.color}`}>
//                             {optionData.option}
//                           </p>
//                         </div>
//                       ))}
//                     </PopoverContent>
//                   </Popover>
//                 </td>
//               </tr>
//             ))}
//           </tbody>
//         </table>
//       </Card>
//     </div>
//   );
// };

// export default Component;
