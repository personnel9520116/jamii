"use client";
import AdminBlog from "@/components/myComponent/AdminBlog";
import CustomButton from "@/components/myComponent/CustomButton";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { Post } from "@/types/type";
import Loader from "@/components/myComponent/Loader";

const AdminBlogPage = () => {
  const [posts, setPosts] = useState<Post[]>([]);
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await fetch("/api/post");
        if (!response.ok) {
          throw new Error("Failed to fetch posts");
        }
        const data = await response.json();
        setPosts(data);
        setIsLoading(false);
      } catch (error) {
        console.error("Error fetching posts:", error);
      }
    };
    fetchPosts();
  }, []);
  if (isLoading)
    return (
      <div className="h-[100vh] w-full flex items-center justify-center">
        <Loader />
      </div>
    );
  return (
    <div>
      <div className="flex justify-between items-center">
        <p className="font-bold text-[24px] text-noir">Mon blog</p>
        <CustomButton
          onClick={() => router.push("/admin/blog/add")}
          text="Nouvel article"
          image="/AddArticle.svg"
          width="202px"
        />
      </div>
      <div className="mt-[32px]">
        <p className="text-lg text-secondary">
          Tous mes articles
          <span className="text-sm text-gris"> {posts.length} articles</span>
        </p>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-2 md:gap-5 pt-5">
        {posts.map((infos, index) => (
          <AdminBlog
            key={infos.id}
            blogData={{
              id: infos.id,
              category: infos.category as string,
              title: infos.title,
              description: infos.description,
              image: infos.image,
              auteur: {
                avatar: infos.author.image as string,
                nom: infos.author.name as string,
              },
              createAt: infos.createdAt,
              partage: infos.share as boolean,
            }}
            onBlog
          />
        ))}
      </div>
    </div>
  );
};

export default AdminBlogPage;
