"use client";
import Input from "@/components/myComponent/Input ";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import "../../add/quill.css";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import CustomButton from "@/components/myComponent/CustomButton";
import { useRouter } from "next/navigation";
import { Switch } from "@/components/ui/switch";
import { Label } from "@/components/ui/label";
import toBase64 from "@/lib/toBase64";
import { toast } from "react-toastify";
import { Post } from "@/types/type";
import Loader from "@/components/myComponent/Loader";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

const modules = {
  toolbar: [
    [{ header: "1" }, { header: "2" }, { font: [] }],
    [{ size: [] }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image", "video"],
    ["clean"],
  ],
  clipboard: {
    matchVisual: false,
  },
};

const formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
];

const EditBlogPage = ({ params }: { params: { id: string } }) => {
  const [post, setPost] = useState<Post | null>(null);
  const router = useRouter();
  const [value, setValue] = useState<string>("");
  const [share, setShare] = useState<boolean>(false);
  const [srcUrl, setSrcUrl] = useState<string | undefined>(undefined);
  const [photoUrl, setPhotoUrl] = useState("");
  const [isClient, setIsClient] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue: setFormValue,
    reset,
  } = useForm<FieldValues>();

  useEffect(() => {
    setIsClient(true);
    const fetchPost = async () => {
      try {
        const response = await fetch(`/api/post/${params.id}`);
        if (!response.ok) {
          throw new Error("Failed to fetch post");
        }
        const data = await response.json();
        setPost(data);
        reset({
          titre: data.title,
          description: data.description,
          metat: data.metaTitle,
          metad: data.metaDescription,
        });
        setValue(data.content || "");
        setShare(data.share || false);
        setPhotoUrl(data.image || "");
        setSrcUrl(data.image || "");
      } catch (error) {
        console.error("Error fetching post:", error);
        toast.error("Erreur lors du chargement de l'article");
      }
    };
    fetchPost();
  }, [params.id, reset]);
  console.log(post);
  const handleChangeFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files![0];
    if (e.target?.files?.[0]) {
      try {
        const base64 = await toBase64(e.target.files[0]);
        setSrcUrl(base64);
        setPhotoUrl(URL.createObjectURL(file));
      } catch (error) {
        toast.error("Erreur lors du chargement de l'image");
      }
    }
  };
  console.log(photoUrl);
  const onSubmit: SubmitHandler<FieldValues> = async (formData) => {
    try {
      const updatedPost = {
        ...post,
        title: formData.titre,
        description: formData.description,
        metaTitle: formData.metat,
        metaDescription: formData.metad,
        content: value,
        share: share,
        image: srcUrl,
      };

      const response = await fetch(`/api/post/${params.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedPost),
      });

      if (!response.ok) {
        throw new Error("Failed to update post");
      }

      toast.success("Article mis à jour avec succès");
      router.push("/admin/blog");
    } catch (error) {
      console.error("Error updating post:", error);
      toast.error("Erreur lors de la mise à jour de l'article");
    }
  };

  if (!post)
    return (
      <div className="h-[100vh] w-full flex items-center justify-center">
        <Loader />
      </div>
    );

  return (
    <div className="mr-[50px]">
      <div className=" flex justify-start items-center">
        <p className=" font-bold text-[24px] text-noir">Ajouter un article</p>
      </div>
      <div className=" flex mt-[32px] gap-[148px]">
        <div>
          <p className=" font-semibold text-[#00303F]">Détails</p>
          <p className=" text-[#96BCC5] pt-3 w-[223px]">
            Titre, Sous titre, Description, images...
          </p>
        </div>
        <div className="flex flex-col w-full gap-5">
          <div
            className={`shadow-[0px_3px_5px_0px_#2F404A26] max-h-[200px] ${
              photoUrl ? "" : " py-[46px]"
            } rounded-[10px] overflow-hidden flex flex-col items-center gap-3 relative`}
          >
            {photoUrl ? (
              <Image
                src={photoUrl}
                draggable={false}
                alt=""
                height={48}
                width={48}
                className="object-cover w-full h-full aspect-square"
              />
            ) : (
              <div>
                <Image
                  draggable={false}
                  src="/img.svg"
                  alt=""
                  height={48}
                  width={48}
                />
                <p>Charger une image de couverture ou glissez déposer.</p>
              </div>
            )}
            <input
              type="file"
              accept="image/*"
              onChange={handleChangeFile}
              className="top-0 opacity-0 w-full h-full border absolute z-10 cursor-pointer"
            />
          </div>
          <div className=" flex flex-col gap-5">
            <Input
              placeholder={"Titre de votre article"}
              type={"text"}
              register={register}
              errors={errors}
              required
              id={"titre"}
              className={" rounded !pl-6 "}
            />
            <Input
              placeholder={"Description"}
              type={"text"}
              register={register}
              errors={errors}
              required
              id={"description"}
              className={" rounded !pl-6 "}
            />
            <div>
              <p className=" font-serif">Contenus</p>
              {isClient && (
                <ReactQuill
                  theme="snow"
                  value={value}
                  onChange={setValue}
                  modules={modules}
                  formats={formats}
                  placeholder="Ecrivez votre article"
                />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className=" flex mt-[32px] gap-[148px] ">
        <div>
          <p className=" font-semibold text-[#00303F]">Propriétés</p>
          <p className=" text-[#96BCC5] pt-3 w-[223px]">
            Fonction additionnelles, attributs, meta description...
          </p>
        </div>

        <div className=" flex flex-col w-full gap-5">
          <Input
            placeholder={"Meta titre"}
            type={"text"}
            register={register}
            errors={errors}
            required
            id={"metat"}
            className={" rounded !pl-6 "}
          />
          <Input
            placeholder={"Meta description"}
            type={"textarea"}
            register={register}
            errors={errors}
            required
            id={"metad"}
            className={" rounded !pl-6 "}
          />
          <div className=" w-full mt-[32px]">
            <div className="flex items-center space-x-8">
              <Switch
                id="share"
                checked={share}
                onCheckedChange={() => setShare(!share)}
              />
              <Label htmlFor="share" className=" font-semibold">
                Autoriser le partage
              </Label>
            </div>
            <div className="flex justify-center mt-[72px]">
              <CustomButton
                onClick={handleSubmit(onSubmit)}
                image="/save.svg"
                className=" font-semibold"
                text="Enregistrer et publier"
                width="464px"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditBlogPage;
