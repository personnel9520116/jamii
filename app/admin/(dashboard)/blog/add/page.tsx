"use client";
import Input from "@/components/myComponent/Input ";
import Image from "next/image";
import React, { useState } from "react";
// import ReactQuill from "react-quill";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import "./quill.css";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import CustomButton from "@/components/myComponent/CustomButton";
import { useRouter } from "next/navigation";
import { Switch } from "@/components/ui/switch";
import { Label } from "@/components/ui/label";
import toBase64 from "@/lib/toBase64";
import { toast } from "react-toastify";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
const modules = {
  toolbar: [
    [{ header: "1" }, { header: "2" }, { font: [] }],
    [{ size: [] }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image", "video"],
    ["clean"],
  ],
  clipboard: {
    matchVisual: false,
  },
  // imageResize: {
  //   displaySize: true,
  // },
};

const formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
];

const AddBlog = () => {
  const router = useRouter();
  const [srcUrl, setSrcUrl] = useState<string | undefined>(undefined);
  const [photoUrl, setPhotoUrl] = useState("");
  const handleChangeFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files![0];
    if (e.target?.files?.[0]) {
      try {
        const base64 = await toBase64(e.target.files[0]);
        setSrcUrl(base64);
        setPhotoUrl(URL.createObjectURL(file));
      } catch (error) {
        toast.error("Erreur lors du chargement de l'image");
      }
    }
  };

  const [value, setValue] = useState<string>("");
  const [share, setShare] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({});
  const onSubmit: SubmitHandler<FieldValues> = async (values) => {
    try {
      const response = await fetch("/api/post", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          data: {
            image: srcUrl,
            share: share,
            content: value,
            slug: values.titre,
            title: values.titre,
            description: values.description,
            metaDescription: values.metad,
            metaTitle: values.metat,
          },
        }),
      });
      if (response.ok) {
        toast.success("Article créé avec succès");
        router.push("/admin/blog");
      } else {
        toast.error("Erreur lors de la création de l'article");
      }
    } catch (error) {
      console.error("Erreur:", error);
    }
  };
  return (
    <div className="mr-[50px]">
      <div className=" flex justify-start items-center">
        <p className=" font-bold text-[24px] text-noir">Ajouter un article</p>
      </div>
      <div className=" flex mt-[32px] gap-[148px]">
        <div>
          <p className=" font-semibold text-[#00303F]">Détails</p>
          <p className=" text-[#96BCC5] pt-3 w-[223px]">
            Titre, Sous titre, Description, images...
          </p>
        </div>
        <div className="flex flex-col w-full gap-5">
          <div
            className={`shadow-[0px_3px_5px_0px_#2F404A26] max-h-[200px] ${
              photoUrl ? "" : " py-[46px]"
            } rounded-[10px] overflow-hidden flex flex-col items-center gap-3 relative`}
          >
            {photoUrl ? (
              <Image
                src={photoUrl}
                draggable={false}
                alt=""
                height={48}
                width={48}
                className="object-cover w-full h-full aspect-square"
              />
            ) : (
              <>
                <Image
                  draggable={false}
                  src="/img.svg"
                  alt=""
                  height={48}
                  width={48}
                />
                <p>Charger une image de couverture ou glissez déposer.</p>
              </>
            )}
            <input
              type="file"
              accept="image/*"
              onChange={handleChangeFile}
              className="top-0 opacity-0 w-full h-full border absolute z-10 cursor-pointer"
            />
          </div>
          <div className=" flex flex-col gap-5">
            <Input
              placeholder={"Titre de votre article"}
              type={"text"}
              register={register}
              errors={errors}
              required
              id={"titre"}
              className={" rounded !pl-6 "}
            />
            <Input
              placeholder={"Description"}
              type={"text"}
              register={register}
              errors={errors}
              required
              id={"description"}
              className={" rounded !pl-6 "}
            />
            <div>
              <p className=" font-serif">Contenus</p>
              {typeof window !== "undefined" && (
                <ReactQuill
                  theme="snow"
                  value={value}
                  onChange={setValue}
                  modules={modules}
                  formats={formats}
                  placeholder="Ecrivez votre article"
                />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className=" flex mt-[32px] gap-[148px] ">
        <div>
          <p className=" font-semibold text-[#00303F]">Propriétés</p>
          <p className=" text-[#96BCC5] pt-3 w-[223px]">
            Fonction additionnelles, attributs, meta description...
          </p>
        </div>

        <div className=" flex flex-col w-full gap-5">
          <Input
            placeholder={"Meta titre"}
            type={"text"}
            // design={"mt-[25px]"}
            // name={input.name}
            register={register}
            errors={errors}
            required
            id={"metat"}
            className={" rounded !pl-6 "}
          />
          <Input
            placeholder={"Meta description"}
            type={"textarea"}
            // design={"mt-[25px]"}
            // name={input.name}
            register={register}
            errors={errors}
            required
            id={"metad"}
            className={" rounded !pl-6 "}
          />
          <div className=" w-full mt-[32px]">
            <div className="flex items-center space-x-8">
              <Switch
                id="share"
                checked={share}
                onCheckedChange={() => setShare(!share)}
              />
              <Label htmlFor="share" className=" font-semibold">
                Autoriser le partage
              </Label>
            </div>
            <div className="flex justify-center mt-[72px]">
              <CustomButton
                onClick={handleSubmit(onSubmit)}
                image="/save.svg"
                className=" font-semibold"
                text="Enregistrer et publier"
                width="464px"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddBlog;
