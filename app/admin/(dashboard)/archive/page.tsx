import AdminBlog from "@/components/myComponent/AdminBlog";
import CustomButton from "@/components/myComponent/CustomButton";
import React from "react";

const AdminArchive = () => {
  const blogData = [
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor",
      description:
        "Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
  ];
  return (
    <div>
      <div className=" flex justify-between items-center">
        <p className=" font-bold text-[24px] text-noir">Mes archives</p>
        <CustomButton
          text="Tous désarchiver"
          image="/Desarchiver.svg"
          width="202px"
          bgcolor="#0C8B28"
          color="text-white"
        />
      </div>
      <div className="mt-[32px]">
        <p className=" text-gris"> 23 projets</p>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-2 md:gap-5 pt-5">
        {blogData.map((infos, index) => (
          <AdminBlog blogData={infos} key={index} />
        ))}
      </div>
    </div>
  );
};

export default AdminArchive;
