"use client";
import CardBlog from "@/components/myComponent/CardBlog";
import CustomButton from "@/components/myComponent/CustomButton";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Card, CardContent } from "@/components/ui/card";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Autoplay } from "swiper/modules";
import { Post } from "@/types/type";
import Loader from "@/components/myComponent/Loader";

const Blog = () => {
  const [posts, setPosts] = useState<Post[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    const fetchPosts = async () => {
      // setIsLoading(true);
      try {
        const response = await fetch("/api/post");
        if (!response.ok) {
          throw new Error("Failed to fetch posts");
        }
        const data = await response.json();
        setPosts(data);
      } catch (error) {
        console.error("Error fetching posts:", error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchPosts();
  }, []);
  const categorie = [
    "Tous les articles",
    "My scholarship",
    "Gestion des déchets",
    "Just ball",
    "AFYA",
  ];
  const blogData = [
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
    {
      category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      partage: true,
    },
  ];
  if (isLoading) {
    return (
      <div className="h-[100vh] w-full flex items-center justify-center">
        <Loader />
      </div>
    );
  }
  return (
    <main>
      <div className=" bg-bgBlogRes lg:bg-bgBlog bg-cover bg-center md:bg-right h-[509px] md:h-[415px] px-11 text-blanch bg-no-repeat flex flex-col items-center justify-center">
        <h2 className=" font-bold text-[48px] md:text-[64px] text-center">
          Bienvenue dans notre blog
        </h2>
        <p className=" text-center text-base py-[40px] md:w-[500px] md:py-4">
          {
            "En t'inscrivant, tu recevras une carte de membre qui te permettra de faire partie de la communauté de JAMII et d'ainsi bénéficier de nombreux avantages"
          }
        </p>
        <div className=" md:hidden">
          <CustomButton text="Rejoignez nous" width="500px" />
        </div>
      </div>
      <div className=" hidden bg-blanch py-[64px] md:flex items-center gap-x-5 justify-center">
        {categorie.map((categorie, index) => (
          <button key={index} className=" text-lg ">
            {categorie}
          </button>
        ))}
      </div>
      <div className=" lg:flex lg:gap-x-11 py-[106px] lg:pl-[92px] xl:pr-[198px]">
        <div className=" hidden lg:block">
          <div>
            <h3 className=" text-xl font-bold pb-4">Post récents</h3>
            <Card className=" flex border-none w-[339px]">
              <Image
                src={"/Jamii President.png"}
                alt="number"
                width={91}
                height={102}
              />
              <CardContent className="p-3">
                <h3 className=" text-sm font-semibold pb-2">
                  Lorem ipsum sit amet dolor consectur adisciping elit
                </h3>
                <CustomButton text="Voir plus" width="106px" height="33px" />
              </CardContent>
            </Card>
          </div>
          <div className=" bg-noir px-5 py-8 rounded text-gris mt-[86px] grid gap-y-5">
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
          </div>
        </div>
        <div className="">
          <div className=" relative h-[392px] lg:h-[376px] lg:rounded-[10px] overflow-hidden">
            <img
              src={"/blog1.png"}
              alt=""
              style={{
                width: "100%",
                height: "100%",
                objectFit: "cover",
                aspectRatio: "16/9",
              }}
            />
            <div className=" absolute bottom-0 p-4 md:p-7 w-full text-blanch ">
              <p className={`  pb-3 `}>AFYA</p>
              <h2 className=" font-bold text-sm md:text-[20px]">
                Lorem ipsum sit amet dolor consectur adisciping elit
              </h2>
              <p className={`text-base pt-3`}>
                {
                  "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt."
                }
              </p>
              <div className=" flex items-center justify-between pt-5">
                <div className={`${" flex gap-x-2 items-center "} text-xs`}>
                  <div className=" flex gap-x-2  items-center">
                    <Avatar className={`${"h-7 w-7"}`}>
                      <AvatarImage src="/Jamii President.png" alt="test" />
                      <AvatarFallback>cn</AvatarFallback>
                    </Avatar>
                    <p>Oufa Ndezi </p>
                    <p className=" hidden md:block">-</p>
                  </div>
                  <p>11 fév. 2024 à 14:58</p>
                </div>
                <button>
                  <Image src="/share.svg" alt="" width={41} height={41} />
                </button>
              </div>
            </div>
          </div>
          <div className="px-4">
            <div className=" lg:hidden mt-8">
              <h3 className=" text-xl font-bold pb-4">Post récents</h3>
              <Swiper
                className="mySwiper"
                spaceBetween={10}
                autoplay={{
                  delay: 2500,
                  disableOnInteraction: false,
                }}
                modules={[Autoplay]}
              >
                {[0, 1, 2, 3, 4].map((index) => (
                  <SwiperSlide key={index}>
                    <Card className=" flex border-none w-[339px]">
                      <Image
                        src={"/Jamii President.png"}
                        alt="number"
                        width={91}
                        height={102}
                      />
                      <CardContent className="p-3">
                        <h3 className=" text-sm font-semibold pb-2">
                          Lorem ipsum sit amet dolor consectur adisciping elit
                        </h3>
                        <CustomButton
                          text="Voir plus"
                          width="106px"
                          height="33px"
                        />
                      </CardContent>
                    </Card>
                  </SwiperSlide>
                ))}
              </Swiper>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-2 md:gap-5 pt-5">
              {posts.map((infos, index) => (
                <CardBlog onBolg key={index} blogData={infos} />
              ))}
            </div>
          </div>
          <div className=" bg-noir px-5 py-8 lg:hidden text-gris mt-[30px] grid gap-y-5">
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
            <p>Lorem ipsum sit amet dolor ?</p>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Blog;
