"use client";
import CardBlog from "@/components/myComponent/CardBlog";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { useMediaQuery } from "react-responsive";
import { Post } from "@/types/type";
import { useParams } from "next/navigation";
import Loader from "@/components/myComponent/Loader";

const DetailBlog = () => {
  const [details, setDetails] = useState<Post | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const params = useParams();

  useEffect(() => {
    const fetchPostDetails = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(`/api/post/${params.Slug}`);
        if (!response.ok) {
          throw new Error("Failed to fetch post details");
        }
        const data = await response.json();
        setDetails(data);
      } catch (error) {
        console.error("Error fetching post details:", error);
      } finally {
        setIsLoading(false);
      }
    };

    if (params.Slug) {
      fetchPostDetails();
    }
  }, [params.Slug]);

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  const isTablette = useMediaQuery({
    query: "(max-width: 1200px)",
  });
  const isMobile = useMediaQuery({
    query: "(max-width: 600px)",
  });

  function handelReturnPerview(): number {
    if (isDesktopOrLaptop) {
      return 4;
    }
    if (isTablette && !isMobile) {
      return 3.5;
    } else {
      return 1.5;
    }
  }

  const links = ["/X2.svg", "/Facebook2.svg", "/Linkedin2.svg"];

  if (isLoading) {
    return (
      <div className="h-[100vh] w-full flex items-center justify-center">
        <Loader />
      </div>
    );
  }

  if (!details) {
    return <div>Article non trouvé</div>;
  }

  return (
    <div className="mt-[68px] mb-[172px]">
      <div className="px-4 lg:px-[182px]">
        <h2 className="text-[48px] text-center lg:text-left lg:text-[64px] font-black text-noir">
          {details.title}
        </h2>
        <div className="flex items-center justify-between pt-5">
          <div className="flex gap-x-2 items-center">
            <Avatar className="h-[45px] w-[45px]">
              <AvatarImage src={details.author?.image || ""} alt="test" />
              <AvatarFallback>cn</AvatarFallback>
            </Avatar>
            <div className="flex flex-col">
              <p>{details.author?.name}</p>
              <p className="text-sm">
                {new Date(details.createdAt).toLocaleDateString("fr-FR", {
                  day: "2-digit",
                  month: "long",
                  year: "numeric",
                })}{" "}
                à {new Date(details.createdAt).getHours()}h
                {new Date(details.createdAt).getMinutes()}
              </p>
            </div>
          </div>
          <div className="flex gap-x-4 lg:gap-x-10">
            {links.map((link, index) => (
              <button key={index}>
                <Image src={`${link}`} alt="" width={41} height={41} />
              </button>
            ))}
          </div>
        </div>
      </div>
      <div className="relative h-[360px] lg:h-[533px] lg:rounded-[10px] overflow-hidden lg:mx-[90px] my-11">
        <img
          src={details.image}
          alt=""
          style={{
            width: "100%",
            height: "100%",
            objectFit: "cover",
            aspectRatio: "16/9",
          }}
        />
        {details.category && (
          <div className="flex gap-[6px] py-1 px-3 absolute bottom-9 items-center bg-blanch rounded-sm ml-4 lg:ml-24">
            <Image src={"/tag.svg"} alt="" width={15} height={15} />
            <p className="text-lg text-grisp">{details.category}</p>
          </div>
        )}
      </div>
      <div className="px-4 lg:px-[182px] pt-10">
        <h2 className="text-[20px] font-bold text-noir pb-5">
          {"Quelques articles similaires"}
        </h2>
        <div>{/* Swiper component for similar articles */}</div>
      </div>
      <div
        className="px-4 lg:px-[182px]"
        dangerouslySetInnerHTML={{
          __html: details.content as string,
        }}
      />
    </div>
  );
};

export default DetailBlog;
