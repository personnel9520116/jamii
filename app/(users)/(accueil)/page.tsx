"use client";
import { Swiper, SwiperSlide, SwiperRef } from "swiper/react";

// Import Swiper styles
import "swiper/css";

import React, { useEffect, useRef, useState } from "react";
import { useMediaQuery } from "react-responsive";
import CardNumber from "@/components/myComponent/CardNumber";
import CardOrganism from "@/components/myComponent/CardOrganism";
import Cardthree from "@/components/myComponent/Cardthree";
import CustomButton from "@/components/myComponent/CustomButton";
import CustomCard from "@/components/myComponent/CustomCard";
import Nav from "@/components/myComponent/Nav";
import SwiperCard from "@/components/myComponent/SwiperCard";
import Image from "next/image";
import CardBlog from "@/components/myComponent/CardBlog";

export default function Home() {
  const swiperRef = useRef<SwiperRef>(null);
  const ref1 = useRef<HTMLDivElement>(null);
  const ref2 = useRef<HTMLDivElement>(null);
  const ref3 = useRef<HTMLDivElement>(null);
  const ref4 = useRef<HTMLDivElement>(null);

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  const isTablette = useMediaQuery({
    query: "(max-width: 1200px)",
  });
  const isMobile = useMediaQuery({
    query: "(max-width: 600px)",
  });
  function handelReturnPerview(): number {
    if (isDesktopOrLaptop) {
      return 3;
    }
    if (isTablette && !isMobile) {
      return 2;
    } else {
      return 1;
    }
  }
  function handelReturnPerviewTwo(): number {
    if (isDesktopOrLaptop) {
      return 4;
    }
    if (isTablette && !isMobile) {
      return 3.5;
    } else {
      return 1.2;
    }
  }

  function handleNext() {
    if (swiperRef.current) {
      return swiperRef.current.swiper.slideNext();
    }
    return;
  }

  function handlePrev() {
    if (swiperRef.current) {
      return swiperRef.current.swiper.slidePrev();
    }
    return;
  }
  const cardData = [
    {
      image: "/Jamii Education.png",
      title: "L'EDUCATION",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux moyens d’un suivis éducatif.",
    },
    {
      image: "/DevdurableIMG.png",
      title: "LE DEVELOPPEMENT DURABLE",
      subTitle:
        "promouvoir des pratiques respectueuses de l'environnement pour un avenir durable.",
    },
    {
      image: "/BienêtreIMG.png",
      title: "LE BIEN-ETRE",
      subTitle: "promotion de la santé, tant physique que mentale.",
    },
  ];
  const swiperData = [
    {
      icon: "/Ellipse 2.svg",
      image: "/Icone.svg",
      title: "Promotion de l'éducation",
      description:
        "Faciliter l'accès à une éducation de qualité pour tous, en mettant l'accent sur les populations défavorisées ou mal des servies. Mise en place de programmes de bourses d'études, organisation de cours de soutien, création de bibliothèques communautaires, et développement de partenariats avec des établissements éducatifs.",
    },
    {
      icon: "/Ellipse 4.svg",
      image: "/Jamii Dev.svg",
      title: "Développement durable",
      description:
        "Sensibiliser à l'importance du développement durable et encourager des pratiques respectueuses de l'environnement au sein de la communauté. Initiatives de recyclage, campagnes de sensibilisation sur la conservation des ressources, encouragement de pratiques agricoles durables, organisation d'ateliers sur l'efficacité énergétique.",
    },
    {
      icon: "/Ellipse 4 (1).svg",
      image: "/Health Icon.svg",
      title: "Promotion de la santé physique",
      description:
        "Améliorer la santé physique des membres de la communauté en encourageant un mode de vie actif et en fournissant des ressources pour la prévention des maladies. Organisation d'événements sportifs locaux, mise en place deprogrammes d'exercices communautaires, ateliers sur la nutritionet la prévention des maladies",
    },
    {
      icon: "/Ellipse 4 (1).svg",
      image: "/Health Icon.svg",
      title: "Promotion de la santé physique",
      description:
        "Améliorer la santé physique des membres de la communauté en encourageant un mode de vie actif et en fournissant des ressources pour la prévention des maladies. Organisation d'événements sportifs locaux, mise en place deprogrammes d'exercices communautaires, ateliers sur la nutritionet la prévention des maladies",
    },
  ];
  const cardNumberData = [
    {
      image: "/one.svg",
      title: "AGIT",
      description:
        "Il est plus facile d'agir à plusieurs que seul. C'est pourquoi nous voulons créer une communauté via laquelle tout à chacun pourra s'appuyer dès qu'il s'agira de passer de la pensée  à l'action.",
    },
    {
      image: "/two.svg",
      title: "S'ENTRAIDER",
      description:
        "Un groupe humain riche en solidarité et entraide vise à créer une communauté solidaire qui s'entraide sans hésiter et n'a pas peur de demander de l'aide.",
    },
    {
      image: "/three.svg",
      title: "CONNAIT TON HISTOIRE",
      description:
        "Se souvenir de son passé permet d'éviter de le répéter. Mettre en avant l'Histoire à travers diverses activités est essentiel pour mieux appréhender l'avenir.",
    },
    {
      image: "/four.svg",
      title: "VALORISE SES CULTURES",
      description:
        "L'homme sans culture est comme un arbre sans fruit. Valoriser sa culture est essentiel. Les cultures seront au cœur des activités de JAMII.",
    },
  ];
  const projetData = [
    {
      background: "bg-Scholarship",
      title: "My Scholarship",
      description:
        "Ce programme offre des bourses d'études et des opportunités d'apprentissage à des individus talentueux, mais parfois désavantagés, afin de briser les barrières financières qui entravent leur développement personnel et professionnel. C'est une initiative qui renforce la promesse de l'égalité des chances pour tous.",
    },
    {
      background: "bg-Afya",
      title: "AFYA",
      description:
        "L'association encourage le bien-être global en offrant des ressources éducatives, des ateliers et des activités visant à renforcer la santé mentale et physique des individus, tout en combattant la stigmatisation qui entoure ces questions cruciales.",
    },
    {
      background: "bg-GestDechet",
      title: "GESTION DES DECHETS",
      description:
        "JAMII ASBL s'engage activement dans des actions visant à sensibiliser à la préservation de l'environnement et à la promotion de pratiques durables. Le respect de notre planète est au cœur de cette initiative, et l'association œuvre pour un monde plus propre et plus respectueux de la nature.",
    },
    {
      background: "bg-Justball",
      title: "JUST BALL",
      description:
        "Le tournoi de basketball Just Ball' n'est pas simplement une compétition sportive, c'est un véritable catalyseur de l'unité et de la diversité. Les valeurs de fair-play, de tolérance et de respect sont mises en avant, favorisant ainsi un environnement où chaque individu, quelque soit son origine, son sexe ou son âge, peut s'épanouir et contribuer à une atmosphère de camaraderie.",
    },
  ];
  const blogData = [
    {
      title: "Unis pour le bien",
      subTitle:
        "Changer le monde, un pas à la fois, Guide pratique pour s'engager dans l'action caritative...",
      image: "/musee.png",
    },
    {
      title: "Solidarité en action",
      subTitle:
        "Plongez dans les récits captivants de bénévoles passionnés qui consacrent leur temps et leurs compétences p...",
      image: "/musee.png",
    },
    {
      title: "Impact positif",
      subTitle: "Comment une petite action peut changer des vies",
      image: "/musee.png",
    },
    {
      title: "Solidarité en action",
      subTitle:
        "Plongez dans les récits captivants de bénévoles passionnés qui consacrent leur temps et leurs compétences p...",
      image: "/musee.png",
    },
    {
      title: "Solidarité en action",
      subTitle:
        "Plongez dans les récits captivants de bénévoles passionnés qui consacrent leur temps et leurs compétences p...",
      image: "/musee.png",
    },
    {
      title: "Solidarité en action",
      subTitle:
        "Plongez dans les récits captivants de bénévoles passionnés qui consacrent leur temps et leurs compétences p...",
      image: "/musee.png",
    },
  ];
  return (
    <>
      <main className=" bg-firstSection bg-cover bg-right w-full h-[676px] pb-[135px] px-4 lg:pl-[180px] lg:pr-[97px] flex flex-col-reverse lg:flex-row items-center justify-center lg:items-end  bg-no-repeat">
        <div className="  flex items-center lg:items-end justify-center lg:justify-between w-full">
          {/* <div className="  flex items-end w-full"> */}
          <div className=" w-[450px]">
            <h1 className=" text-white text-[64px] font-black">JAMII ASBL</h1>
            <h3 className=" text-primary">
              un mot en swahili qui signifie « communauté ».
            </h3>
            <p className=" text-white mt-4">
              {
                "est une association belge dynamique et engagée qui incarne la vision d'une société éducative, culturelle et socialement inclusive."
              }
            </p>
            <div className=" flex flex-col lg:flex-row mt-5 gap-4">
              <CustomButton
                text="Rejoingnez-nous"
                image="/Jamii Join Ico.svg"
              />
              <CustomButton
                text="Contactez nous"
                image="/Jamii Phone.svg"
                bgcolor="#00303F"
                color="text-blanch"
              />
            </div>
          </div>
        </div>
        <div className=" fixed z-50 right-[98px] hidden lg:block pb-[45px]">
          <Nav ref1={ref1} ref2={ref2} ref3={ref3} ref4={ref4} />
        </div>
      </main>
      <div
        ref={ref1}
        className="px-4 lg:px-[180px] flex flex-col items-center py-[80px]"
      >
        <h3 className=" uppercase font-bold text-[25px] md:text-[32px] mb-[40px] text-center md:px-[400px]">
          Les piliers fondamentaux de JAMII ASBL
        </h3>
        <p className=" text-center mb-5">
          {" sont représentés par trois programmes qui s'articulent autour de"}
        </p>
        <div className=" flex md:gap-x-5 md:flex-row flex-col gap-y-5 md:gap-y-0">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} />
          ))}
        </div>
        <p className=" font-semibold text-center mt-[30px] lg:px-[200px]">
          {
            "L'association s'efforce de répondre aux besoins essentiels de la société d'aujourd'hui et de demain, en insufflant des valeurs d'inclusion et de cohésion sociale."
          }
        </p>
      </div>
      <div className=" bg-noir pl-4 lg:pl-[180px] py-[50px]">
        <h3 className=" uppercase font-bold text-[32px] text-blanch mb-[40px] text-center ">
          Nos objectifs
        </h3>
        <div className="  relative">
          <Swiper
            ref={swiperRef}
            className="mySwiper"
            slidesPerView={handelReturnPerview()}
            spaceBetween={10}
            autoplay
          >
            {swiperData.map((infos, index) => (
              <SwiperSlide key={index}>
                <SwiperCard infoData={infos} />
              </SwiperSlide>
            ))}
          </Swiper>
          <div className=" absolute flex justify-between items-center w-full top-[50%] z-50 lg:pr-[180px]">
            <button onClick={handlePrev}>
              <Image
                src={"/row.svg"}
                className=" -rotate-180 cursor-pointer"
                alt=""
                height={58}
                width={58}
              />
            </button>
            <button onClick={handleNext}>
              <Image
                src={"/row.svg"}
                alt=""
                className=" cursor-pointer"
                height={58}
                width={58}
              />
            </button>
          </div>
        </div>
      </div>
      <div className="px-4 lg:px-[180px] pt-[50px]" ref={ref2}>
        <h3 className=" uppercase font-bold text-[32px] mb-[40px] text-center ">
          Nos projets en cours
        </h3>
        <div className=" flex flex-col gap-y-[40px]">
          {projetData.map((infos, index) => (
            <Cardthree key={index} index={index} projectCardData={infos} />
          ))}
        </div>
        <div className=" flex justify-center py-[50px]">
          <CustomButton
            text="Rejoingnez-nous"
            image="/Jamii Join Ico.svg"
            width=" w-[367px]"
            className=" bg-transparent border-[1.5px] border-secondary"
          />
        </div>
      </div>
      <div ref={ref3}>
        <h3 className=" uppercase font-bold text-[32px] mb-[40px] text-center ">
          Notre blog
        </h3>
        <div className="h-[500px]">
          <Swiper
            enabled
            slideActiveClass="scale-125"
            className="mySwiper"
            spaceBetween={10}
            slidesPerView={handelReturnPerviewTwo()}
          >
            {blogData.map((infos, index) => (
              <SwiperSlide key={index}>
                <CustomCard cardData={infos} ishome bgColor="text-blanch" />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        <div className=" flex justify-center">
          <CustomButton
            text="Rejoingnez-nous"
            color="text-blanch"
            width=" w-[367px]"
            className="bg-secondary"
          />
        </div>
      </div>
      {/* <div className="bg-noir px-[435px] py-[50px]">
        <h2 className=" uppercase font-bold text-[32px] text-blanch mb-[40px] text-center ">
          Organisation Interne
        </h2>
        <div className=" flex flex-col gap-y-[100px]">
          <CardOrganism description="ter" />
          <CardOrganism description="ter" />
        </div>
        <div className=" flex gap-x-5 mt-[100px]">
          <CardOrganism />
          <CardOrganism />
          <CardOrganism />
          <CardOrganism />
        </div>
      </div> */}
      <div className=" px-4 lg:px-[180px] py-[50px]" ref={ref4}>
        <div className=" flex flex-col gap-y-6 md:gap-y-6 lg:flex-row lg:gap-x-[30px] lg:pr-[92px]">
          {cardNumberData.slice(0, 2).map((infos, index) => (
            <CardNumber key={index} cardData={infos} />
          ))}
        </div>
        <div className=" flex flex-col gap-y-6 md:gap-y-6 lg:flex-row lg:gap-x-[30px] lg:pl-[92px] pt-6">
          {cardNumberData.slice(2, 4).map((infos, index) => (
            <CardNumber key={index} cardData={infos} />
          ))}
        </div>
      </div>
    </>
  );
}
