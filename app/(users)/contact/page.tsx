"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import React from "react";
import Image from "next/image";
import FormulaireCard from "@/components/myComponent/FormulaireCard";

const ContactPage = () => {
  const contactInfo = [
    { image: "/Call_ico.svg", title: "+32 471 52 32 20" },
    { image: "/mail_ico2.svg", title: "info@jamii.com" },
    { image: "/location_Ico.svg", title: "Bruxelles, Belgique" },
  ];
  const Images = ["/x.svg", "/facebook.svg", "/in.svg"];
  return (
    <div>
      <div className=" bg-bgContactRes lg:bg-bgContact bg-cover bg-center lg:px-[182px] pt-[113px] md:bg-right h-[516px] md:h-[415px] px-4 text-blanch bg-no-repeat flex flex-col justify-between pb-[37px] ">
        <div className=" flex flex-col items-center">
          <h2 className=" font-bold text-[48px] md:text-[64px] md:text-center">
            Contactez nous
          </h2>
          <p className=" md:text-center text-base py-[40px] md:w-[612px] md:py-4">
            {
              "Rejoignez JAMII ASBL pour faire la différence dans la préservation de l'environnement et la promotion d'un mode de vie respectueux de la nature."
            }
          </p>
        </div>
        <div className=" flex gap-5 justify-center  md:justify-start ">
          {Images.map((image, index) => (
            <Image key={index} src={`${image}`} alt="" width={45} height={45} />
          ))}
        </div>
      </div>
      <div className=" lg:relative">
        <div className=" hidden lg:block">
          <div className=" px-[92px] flex bg-blanch pt-[180px] pb-[110px]">
            <div className=" flex flex-col gap-8 w-[40%]">
              {contactInfo.map((info, index) => (
                <div
                  key={index}
                  className=" flex items-center gap-3 bg-noir py-[10px] px-5 rounded-[35px]"
                >
                  <div className=" bg-secondary rounded-full h-[45px] w-[45px] flex items-center justify-center">
                    <Image src={info.image} alt="" height={20} width={20} />
                  </div>
                  <p className=" text-grisf">{info.title}</p>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className=" md:px-[100px] pt-6 lg:w-[50%] lg:absolute top-[-140px] right-[90px]">
          <FormulaireCard />
        </div>
      </div>
      <div className=" px-[100px] lg:px-[182px] pt-[150px] lg:pt-[286px] pb-[164px] hidden md:block ">
        <h3 className=" text-center uppercase font-bold text-[32px]">
          retrouvez nous sur google maps
        </h3>
        <p className=" text-center pt-5 pb-[50px]">
          {
            "Explorez notre emplacement et découvrez notre engagement environnemental sur Google Maps"
          }
        </p>
        <img src="/map.png" alt="" className=" w-full " />
      </div>
    </div>
  );
};

export default ContactPage;
