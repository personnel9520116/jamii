"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import CustomCard from "@/components/myComponent/CustomCard";
import { Calendar } from "@/components/ui/calendar";
import Image from "next/image";
import React from "react";

const DetailAgendaPage = () => {
  const [date, setDate] = React.useState<Date | undefined>(new Date());
  const cardData = [
    {
      image: "/Jamii Education.png",
      title: "L'EDUCATION",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux moyens d’un suivis éducatif.",
    },
    {
      image: "/DevdurableIMG.png",
      title: "LE DEVELOPPEMENT DURABLE",
      subTitle:
        "promouvoir des pratiques respectueuses de l'environnement pour un avenir durable.",
    },
    {
      image: "/BienêtreIMG.png",
      title: "LE BIEN-ETRE",
      subTitle: "promotion de la santé, tant physique que mentale.",
    },
  ];
  return (
    <main>
      <div className=" bg-etudiante bg-cover bg-center md:bg-right h-[509px] md:h-[415px] px-11 lg:px-[90px] xl:px-[182px] text-blanch bg-no-repeat flex flex-col items-center justify-center">
        <h2 className=" font-bold text-[48px] md:text-[64px] text-center">
          My Scholarship
        </h2>
        <p className=" text-center text-base py-[40px] md:py-4">
          {
            "L'association s'engage à promouvoir le bien-être global de ses membres en leur fournissant des ressources éducatives pour les aider à mieux comprendre et gérer leur santé mentale et physique. Des ateliers sont également organisés pour leur permettre d'acquérir des compétences et des outils pratiques pour améliorer leur bien-être au quotidien."
          }
        </p>
        <CustomButton
          text="Du 16 Déc. 2023 au 13 Mars 2024"
          width="500px"
          bgcolor="#00303F"
          color=" text-blanch"
          className=" opacity-50"
        />
      </div>
      <div className="px-4 py-10 lg:px-[90px] xl:px-[182px]">
        <div>
          <div className=" flex items-center flex-col md:flex-row gap-x-8">
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <div>
              <h2 className=" text-[32px] font-bold pb-5">Lorem ipsum</h2>
              <p>
                {
                  " Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet"
                }
              </p>
            </div>
          </div>
          <div className=" grid grid-cols-2 md:grid-cols-4 gap-5 pt-8">
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
          </div>
        </div>
        <div>
          <div className=" flex items-center flex-col md:flex-row-reverse pt-8 gap-x-8">
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <div>
              <h2 className=" text-[32px] font-bold pb-5">Lorem ipsum</h2>
              <p>
                {
                  " Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet"
                }
              </p>
            </div>
          </div>
          <div className=" grid grid-cols-2 md:grid-cols-4 gap-5 pt-8">
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
            <img src="/blog.png" alt="" className=" rounded-[10xp]" />
          </div>
        </div>
      </div>
      <div className="px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className="font-bold text-[25px] md:text-[32px] text-center ">
          Quelques articles sur Scholarship
        </h2>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-5 pt-8">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} />
          ))}
        </div>
      </div>
      <div className=" lg:mx-[182px] lg:mb-10 bg-noir px-4 md:rounded-[10px] lg:px-[90px] mt-12 xl:pl-[54px] xl:pr-[28px] flex flex-col md:flex-row text-blanch items-center py-[28px]">
        <div>
          <h3 className=" text-[32px] font-bold">Durée du projet</h3>
          <p className=" pt-5 pb-8">
            {
              " Le projet s’étend sur une période de 28 jours dans le mois de mars Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            }
          </p>
          <div className=" hidden md:block">
            <CustomButton
              text="Faire un don"
              width="235px"
              image="/Jamii gift.svg"
            />
          </div>
        </div>
        <div>
          <Calendar
            mode="single"
            selected={date}
            onSelect={setDate}
            className="rounded-md bg-[#FBFEFF33] !w-full"
          />
        </div>
        <div className=" md:hidden mt-8">
          <CustomButton
            text="Faire un don"
            width="100%"
            image="/Jamii gift.svg"
          />
        </div>
      </div>
      {/* <div className="px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-[25px] md:text-[32px] text-center">
          Nos articles de blog
        </h2>
        <div className="grid grid-cols-2 lg:grid-cols-3 gap-2 md:gap-5 pt-10">
          <CardBlog />
          <CardBlog />
          <CardBlog />
          <CardBlog />
          <CardBlog />
          <CardBlog />
        </div>
      </div> */}
    </main>
  );
};

export default DetailAgendaPage;
