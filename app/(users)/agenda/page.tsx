"use client";
import CardBlog from "@/components/myComponent/CardBlog";
import CustomButton from "@/components/myComponent/CustomButton";
import CustomCard from "@/components/myComponent/CustomCard";
import { Post } from "@/types/type";
import React, { useEffect, useState } from "react";

const AgendaPage = () => {
  const cardData = [
    {
      image: "/noirbaby.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
    {
      image: "/ecoliere.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
    {
      image: "/reunion.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
    {
      image: "/boy.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
    {
      image: "/jouet.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
    {
      image: "/masque.png",
      title: "Lorem ipsum sit amet dolor",
      subTitle:
        "Aider les personnes défavorisées à s'intégrer dans la société aux.",
    },
  ];
  const blogData = [
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/musee.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
    {
      // category: "AFYA",
      title: "Lorem ipsum sit amet dolor consectur adisciping elit",
      description:
        "Lorem ipsum sit amet dolor consectur adisciping elit Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
      image: "/reunion.png",
      auteur: {
        avatar: "/Jamii President.png",
        nom: "Oufa Ndezi ",
      },
      createAt: "11 fév. 2024 à 14:58",
      // partage: true,
    },
  ];
  const [posts, setPosts] = useState<Post[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    const fetchPosts = async () => {
      // setIsLoading(true);
      try {
        const response = await fetch("/api/post");
        if (!response.ok) {
          throw new Error("Failed to fetch posts");
        }
        const data = await response.json();
        setPosts(data);
      } catch (error) {
        console.error("Error fetching posts:", error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchPosts();
  }, []);
  return (
    <main>
      <div className=" bg-agendabg bg-cover bg-center md:bg-right h-[509px] md:h-[415px] px-11 text-blanch bg-no-repeat flex flex-col items-center justify-center">
        <h2 className=" font-bold text-[48px] md:text-[64px] text-center">
          Nos évènements
        </h2>
        <p className=" text-center text-base py-[40px] md:w-[500px] md:py-4">
          {
            "Ne manquez plus aucune occasion de vous divertir et de vivre des moments uniques en notre compagnie. Rejoignez-nous pour une expérience inoubliable !"
          }
        </p>
        <CustomButton
          text="Notre calendrier d’activités"
          width="500px"
          image="/Calendar.svg"
        />
      </div>
      <div className="px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-[25px] md:text-[32px] text-center md:px-[400px]">
          my scholarship
        </h2>
        <p className=" text-sm text-grisp">Du 16 Déc. 2023 au 13 Mars 2024</p>
        <p className=" text-center mt-[5px] mb-5 text-secondary">
          {
            "L'association s'engage à promouvoir le bien-être global de ses membres en leur fournissant des ressources éducatives pour les aider à mieux comprendre et gérer leur santé mentale et physique. Des ateliers sont également organisés pour leur permettre d'acquérir des compétences et des outils pratiques pour améliorer leur bien-être au quotidien."
          }
        </p>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-5">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} bgColor="bg-blanch" />
          ))}
        </div>
      </div>
      <div className=" bg-noir px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-blanch text-[25px] md:text-[32px] text-center md:px-[400px]">
          AFYA
        </h2>
        <p className=" text-sm text-grisp">Du 16 Déc. 2023 au 13 Mars 2024</p>
        <p className=" text-center mt-[5px] mb-5 text-blanch">
          {
            "L'association s'engage à promouvoir le bien-être global de ses membres en leur fournissant des ressources éducatives pour les aider à mieux comprendre et gérer leur santé mentale et physique. Des ateliers sont également organisés pour leur permettre d'acquérir des compétences et des outils pratiques pour améliorer leur bien-être au quotidien."
          }
        </p>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-5">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} bgColor="bg-blanch" />
          ))}
        </div>
      </div>
      <div className="px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-[25px] md:text-[32px] text-center md:px-[400px]">
          gestion des déchets
        </h2>
        <p className=" text-sm text-grisp">Du 16 Déc. 2023 au 13 Mars 2024</p>
        <p className=" text-center mt-[5px] mb-5 text-secondary">
          {
            " JAMII ASBL, à travers ses actions, cherche à sensibiliser le public à l'importance de préserver l'environnement et à promouvoir des pratiques durables. L'association s'engage activement pour sensibiliser les individus aux enjeux environnementaux et les inciter à adopter des comportements respectueux de la nature."
          }
        </p>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-5">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} bgColor="bg-blanch" />
          ))}
        </div>
      </div>
      <div className=" bg-noir px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-blanch text-[25px] md:text-[32px] text-center md:px-[400px]">
          just ball
        </h2>
        <p className=" text-sm text-grisp">Du 16 Déc. 2023 au 13 Mars 2024</p>
        <p className=" text-center mt-[5px] mb-5 text-blanch">
          {
            "Le tournoi de basketball 'Just Ball' va bien au-delà d'une simple compétition sportive. C'est un événement qui agit comme un catalyseur de l'unité et de la diversité au sein de la communauté. En mettant en avant les valeurs fondamentales du fair-play, de la tolérance et du respect, le tournoi crée un environnement propice à l'épanouissement de chaque individu, indépendamment de son origine, de son sexe ou de son âge."
          }
        </p>
        <div className="grid grid-cols-2 md:grid-cols-3 gap-5">
          {cardData.map((infos, index) => (
            <CustomCard key={index} cardData={infos} bgColor="bg-blanch" />
          ))}
        </div>
      </div>
      <div className="px-4 lg:px-[90px] xl:px-[182px] flex flex-col items-center py-[80px]">
        <h2 className=" uppercase font-bold text-[25px] md:text-[32px] text-center">
          Nos articles de blog
        </h2>
        <div className="grid grid-cols-2 lg:grid-cols-3 gap-2 md:gap-5 pt-10">
          {posts.map((infos, index) => (
            <CardBlog key={index} blogData={infos} />
          ))}
        </div>
      </div>
    </main>
  );
};

export default AgendaPage;
