"use client";
import CustomButton from "@/components/myComponent/CustomButton";
import ModalContact from "@/components/myComponent/ModalContact";
import React, { useEffect, useState } from "react";

const Join = () => {
  const [open, setOpen] = useState<boolean>();

  useEffect(() => {
    if (open) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }
  }, [open]);
  return (
    <main>
      <div className=" bg-bgBlogRes lg:bg-bgBlog bg-cover bg-center md:bg-right h-[509px] md:h-[415px] px-11 text-blanch bg-no-repeat flex flex-col items-center justify-center">
        <h2 className=" font-bold text-[48px] md:text-[64px] text-center">
          Rejoins la communauté unis de Jamii
        </h2>
        <p className=" text-center text-base py-[40px] md:w-[500px] md:py-4">
          {
            "En t'inscrivant, tu recevras une carte de membre qui te permettra de faire partie de la communauté de JAMII et d'ainsi bénéficier de nombreux avantages"
          }
        </p>
        <CustomButton text="Rejoignez nous" width="500px" />
      </div>
      <div className=" flex py-10 px-4 md:px-[90px] flex-col lg:flex-row items-center">
        <img
          src="/blog.png"
          alt=""
          className=" relative lg:right-[-200px] h-[430px] rounded-[10px]"
        />
        <div className=" lg:bg-blanch rounded-[10px] lg:pl-[300px] lg:pr-[94px] py-5 lg:py-[85px]">
          <h3 className=" uppercase font-bold text-[25px] lg:text-[32px]">
            Nous rejoindre comme <span className=" text-primary">actif</span>
          </h3>
          <p className=" py-5 lg:py-9">
            {
              " En rejoignant JAMII en tant qu'actif, vous pourrez avoir un impact réel et positif sur l'environnement et la société, tout en vous épanouissant personnellement et en contribuant à la construction d'un avenir plus vert et plus durable pour tous. Rejoignez-nous en tant qu'actif et ensemble, agissons pour un monde meilleur et plus respectueux de notre planète."
            }
          </p>
          <CustomButton
            text="Nous rejoindre"
            width="500px"
            image="/joinIcon.svg"
            onClick={() => setOpen(true)}
          />
        </div>
      </div>
      <div className=" flex py-10 px-4 md:px-[90px] flex-col lg:flex-row-reverse items-center">
        <img
          src="/blog.png"
          alt=""
          className=" relative lg:left-[-200px] h-[430px] rounded-[10px]"
        />
        <div className=" lg:bg-blanch rounded-[10px] lg:pr-[300px] lg:pl-[94px] py-5 lg:py-[85px]">
          <h3 className=" uppercase font-bold text-[25px] lg:text-[32px]">
            Nous rejoindre comme <span className=" text-primary">actif</span>
          </h3>
          <p className=" py-5 lg:py-9">
            {
              "En rejoignant JAMII en tant qu'actif, vous pourrez avoir un impact réel et positif sur l'environnement et la société, tout en vous épanouissant personnellement et en contribuant à la construction d'un avenir plus vert et plus durable pour tous. Rejoignez-nous en tant qu'actif et ensemble, agissons pour un monde meilleur et plus respectueux de notre planète."
            }
          </p>
          <CustomButton
            text="Nous rejoindre"
            width="500px"
            image="/joinIcon.svg"
            onClick={() => setOpen(true)}
          />
        </div>
      </div>
      {open && <ModalContact onClose={() => setOpen(false)} />}
    </main>
  );
};

export default Join;
