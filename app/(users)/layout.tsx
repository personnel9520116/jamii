import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../globals.css";
import Footer from "@/components/myComponent/Footer";
import Header from "@/components/myComponent/Header";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Jamii",
  description: "Site de bénevolat",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className} bg-grisf`}>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  );
}
