export interface Post {
  id: string;
  title: string;
  description: string;
  image: string;
  share: boolean | null;
  metaTitle?: string;
  metaDescription?: string;
  author: {
    id: string;
    name: string | null;
    image: string | null;
  };
  createdAt: Date;
  updatedAt: Date;
  category: string | null;
  slug?: string;
  content?: string;
}
