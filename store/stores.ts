import { create } from "zustand";

type User = {
  name: string;
  image: string;
};
interface UserStore {
  user: User;
  setUser: (user: User) => void;
}

export const useUserStore = create<UserStore>((set) => ({
  user: {
    name: "",
    image: "",
  },
  setUser: (user) => set({ user }),
}));
