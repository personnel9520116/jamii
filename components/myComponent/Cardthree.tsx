import React from "react";
import { Card, CardContent, CardTitle } from "../ui/card";
type projetcardProps = {
  index: number;
  projectCardData: {
    background: string;
    title: string;
    description: string;
  };
};

const Cardthree = ({ projectCardData, index }: projetcardProps) => {
  return (
    <div
      className={`${projectCardData.background} w-full bg-cover bg-right h-[323px] md:bg-right md:h-[420px] rounded-lg bg-no-repeat px-5 py-5 md:px-[38px] md:py-[30px]`}
    >
      <div
        className={` flex flex-col ${
          index === 0 || index === 2 ? " md:flex-row-reverse" : " md:flex-row"
        }  justify-between items-end h-full w-full`}
      >
        <div className="w-full h-[30%]"></div>
        <Card className="w-full md:px-[50px] md:py-[30px] p-4 py-5 rounded-[13px] bg-blanch opacity-80 text-secondary">
          <CardTitle className=" font-bold text-lg md:text-2xl">
            {projectCardData.title}
          </CardTitle>
          <CardContent className="px-0 py-0 pt-2 md:pt-4 md:text-base text-sm ">
            {projectCardData.description}
          </CardContent>
        </Card>
      </div>
    </div>
  );
};

export default Cardthree;
