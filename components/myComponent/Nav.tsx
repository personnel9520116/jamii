import Image from "next/image";
import React, { RefObject } from "react";
interface HeadProps {
  ref1: RefObject<HTMLDivElement>;
  ref2: RefObject<HTMLDivElement>;
  ref3: RefObject<HTMLDivElement>;
  ref4: RefObject<HTMLDivElement>;
}

const Nav = ({ ref1, ref2, ref3, ref4 }: HeadProps) => {
  const scrollToSection = (ref: RefObject<HTMLDivElement>) => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
  };

  const dataImage = [
    { image: "/Home Icon.svg", ref: ref1 },
    { image: "/Jamii Aboutus Icon.svg", ref: ref2 },
    { image: "/Jamii.svg", ref: ref3 },
    { image: "Jamii Bag Icon.svg", ref: ref4 },
  ];
  return (
    <div className=" bg-[#1E273ECC] lg:bg-[#000A21] rounded-[46px] w-[361px] h-[78px] lg:w-[78px] px-5 md:px-0 lg:py-5 lg:h-[361px] items-center justify-between flex lg:flex-col">
      {dataImage.map((image, index) => (
        <div
          key={index}
          ref={image.ref}
          className=" h-[65px] w-[65px] bg-[#00303F] cursor-pointer flex rounded-full items-center justify-center"
          onClick={() => {
            scrollToSection(image.ref);
          }}
        >
          <Image
            key={index}
            src={`${image.image}`}
            alt=""
            width={30}
            height={30}
          />
        </div>
      ))}
    </div>
  );
};

export default Nav;
