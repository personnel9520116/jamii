import React from "react";
interface IconProps {
  className?: string;
}
const DashboardIcon = ({ className }: IconProps) => {
  return (
    <svg
      className={className}
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 1H6.75013V8.66683H1V1Z"
        stroke="#000A21"
        stroke-width="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1 12.5H6.75013V16.3334H1V12.5Z"
        stroke="#000A21"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.5835 8.6665H16.3336V16.3333H10.5835V8.6665Z"
        stroke="#000A21"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.5835 1H16.3336V4.83342H10.5835V1Z"
        stroke="#000A21"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default DashboardIcon;
