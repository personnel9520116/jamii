"use client";
import React, { useEffect, useRef, useState } from "react";
import CustomButton from "./CustomButton";
import Input from "./Input ";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
import { Card, CardTitle } from "../ui/card";
import "./component.css";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
type Props = {
  isModal?: boolean;
};
const FormulaireCard = ({ isModal }: Props) => {
  const selectRef = useRef<any>(null);
  const [country, setCountry] = useState<string>("US");
  const [value, setValue] = useState<string>("+1");
  const [checked, setChecked] = useState<boolean>(false);
  const formInput = [
    {
      id: "nom",
      placeholder: "Votre nom complet",
      type: "text",
      image: "/user.svg",
    },
    {
      id: "mail",
      placeholder: "Votre mail",
      type: "email",
      image: "/email.svg",
    },
    {
      id: "ville",
      placeholder: "Ville de résidence",
      type: "text",
      image: "/ville.svg",
    },
    {
      id: "activite",
      placeholder: "Domaine d’activité",
      type: "text",
      image: "/jobs.svg",
    },
    {
      id: "description",
      placeholder: "Décrivez nous en quelques mots votre besoin",
      type: "textarea",
    },
  ];
  useEffect(() => {
    console.log(value?.slice(4, value?.length));
  }, []);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      mail: "",
      password: "",
    },
  });
  const onSubmit: SubmitHandler<FieldValues> = (values) => {};
  return (
    <Card
      className={` bg-grisf px-4 border-none md:border shadow-lg ${
        isModal ? " lg:px-12" : " lg:px-16"
      } py-[50px]`}
    >
      <CardTitle className=" font-bold text-2xl text-center">
        Ecrivez nous quelques mots à propos de lorem ipsum sit amet dolor.
      </CardTitle>
      {formInput.slice(0, 2).map((input, index) => (
        <div className=" relative" key={index}>
          <Input
            placeholder={input.placeholder}
            type={input.type}
            design={"mt-[25px]"}
            id={input.id}
            register={register}
            errors={errors}
            required
            className={" rounded-[25px]"}
          />
          {input.image && (
            <img
              src={`${input.image}`}
              draggable="false"
              alt=""
              className=" absolute bottom-3 left-6 "
            />
          )}
        </div>
      ))}
      <div className=" relative">
        <p className="absolute cursor-pointer z-20 top-[14px] left-[20px] text-blanch ">
          {value?.slice(0, 4)}
        </p>
        <PhoneInput
          international
          ref={selectRef}
          defaultCountry="US"
          country={country}
          value={value?.slice(4, value?.length)}
          onChange={(value) => {
            setValue(value as string);
          }}
        />
        <img
          src="/select.svg"
          alt=""
          className="absolute cursor-pointer top-[24px] left-[140px]"
        />
      </div>
      {formInput.slice(2, 5).map((input, index) => (
        <div className=" relative" key={index}>
          <Input
            placeholder={input.placeholder}
            type={input.type}
            design={"mt-[25px]"}
            id={input.id}
            register={register}
            errors={errors}
            required
            className={" rounded-[35px]"}
          />
          {input.image && (
            <img
              src={`${input.image}`}
              draggable="false"
              alt=""
              className=" absolute bottom-3 left-6 "
            />
          )}
        </div>
      ))}
      <div className=" mt-11">
        <CustomButton text="Nous contacter" onClick={handleSubmit(onSubmit)} />
      </div>
    </Card>
  );
};

export default FormulaireCard;
