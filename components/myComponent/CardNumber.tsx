import React from "react";
import { Card, CardContent, CardTitle } from "../ui/card";
import Image from "next/image";
type cardNumberprops = {
  cardData: {
    image: string;
    title: string;
    description: string;
  };
};
const CardNumber = ({ cardData }: cardNumberprops) => {
  return (
    <Card className="w-full py-[30px] px-[18px] flex items-center rounded-[13px] bg-blanch opacity-80 text-secondary">
      <Image
        src={`${cardData.image}`}
        alt="number"
        width={66}
        height={61}
        className=" hidden lg:block"
      />
      <div className="pl-[20px]">
        <CardContent className="px-0 py-0">
          <CardTitle className=" font-bold text-2xl text-bleu flex gap-x-2">
            <Image
              src={`${cardData.image}`}
              alt="number"
              width={36}
              height={36}
              className="lg:hidden"
            />
            <h4 className=" text-[18px] md:text-[20px]">{cardData.title}</h4>
          </CardTitle>
          <p className=" text-secondary text-sm pt-[5px]">
            {cardData.description}
          </p>
        </CardContent>
      </div>
    </Card>
  );
};

export default CardNumber;
