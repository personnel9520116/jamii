"use client";
import React from "react";
import { UseFormRegister, FieldValues, FieldErrors } from "react-hook-form";
type Props = {
  label?: string;
  placeholder?: string;
  type?: string;
  className?: string;
  design?: string;
  labed?: string;
  name?: string;
  required?: boolean;
  register: UseFormRegister<FieldValues>;
  errors: FieldErrors;
  disabled?: boolean;
  id: string;
};

const Input = ({
  label,
  placeholder,
  type,
  className,
  design,
  labed,
  name,
  register,
  required,
  errors,
  id,
  disabled,
}: Props) => {
  return (
    <div className={`flex flex-col ${design ? design : ""}`}>
      <label htmlFor={id} className={` text-secondary ${labed ? labed : ""}`}>
        {label}
      </label>

      {type === "textarea" ? (
        <textarea
          id={id}
          {...register(id, { required })}
          disabled={disabled}
          className={` border rounded-[5px]  placeholder:text-gris bg-grisf min-h-[254px] max-h-[254px] outline-none p-5 ${
            className ? className : ""
          } ${errors[id] ? "border-rose-400" : "border-grisp"}`}
          placeholder={placeholder}
        />
      ) : (
        <input
          id={id}
          autoComplete="off"
          disabled={disabled}
          {...register(id, { required })}
          type={type}
          className={` border border-grisp placeholder:text-gris bg-grisf h-12 outline-none pl-[62px] ${
            className ? className : ""
          } ${errors[id] ? "border-rose-400" : "border-grisp"}`}
          placeholder={placeholder}
        />
      )}
    </div>
  );
};

export default Input;
