import React, { useEffect, useRef, useState } from "react";
import FormulaireCard from "./FormulaireCard";
import { Card, CardTitle } from "../ui/card";
import Input from "./Input ";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
import "./join.css";
import CustomButton from "./CustomButton";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";

type Props = {
  onClose: () => void;
};
const ModalContact = ({ onClose }: Props) => {
  const selectRef = useRef<any>(null);
  const [country, setCountry] = useState<string>("US");
  const [value, setValue] = useState<string>("+1");
  const [checked1, setChecked1] = useState<boolean>(false);
  const [checked2, setChecked2] = useState<boolean>(false);
  const [checked3, setChecked3] = useState<boolean>(false);
  const formInput = [
    {
      id: "nom",
      placeholder: "Votre nom complet",
      type: "text",
      image: "/user.svg",
    },
    {
      id: "mail",
      placeholder: "Votre mail",
      type: "email",
      image: "/email.svg",
    },
    {
      id: "ville",
      placeholder: "Ville de résidence",
      type: "text",
      image: "/ville.svg",
    },
    {
      id: "activite",
      placeholder: "Domaine d’activité",
      type: "text",
      image: "/jobs.svg",
    },
  ];
  useEffect(() => {
    console.log(value?.slice(4, value?.length));
  }, []);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      nom: "",
      mail: "",
      ville: "",
      activite: "",
    },
  });
  const onSubmit: SubmitHandler<FieldValues> = (values) => {};
  return (
    <div
      className="flex items-center justify-center lg:justify-end fixed bg-black/70 top-0 bottom-0 px-4 lg:pr-[90px] left-0 right-0 z-20 "
      onClick={onClose}
    >
      <div
        className=" w-[524px] overflow-scroll rounded-md "
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <Card
          className={` bg-grisf px-4 border-none md:border shadow-lg  lg:px-12
           py-[50px]`}
        >
          <CardTitle className=" font-bold text-2xl text-center">
            Ecrivez nous quelques mots à propos de lorem ipsum sit amet dolor.
          </CardTitle>
          {formInput.slice(0, 2).map((input, index) => (
            <div className=" relative" key={index}>
              <Input
                placeholder={input.placeholder}
                type={input.type}
                design={"mt-[25px]"}
                register={register}
                errors={errors}
                required
                id={input.id}
                className={" rounded"}
              />
              {input.image && (
                <img
                  src={`${input.image}`}
                  draggable="false"
                  alt=""
                  className=" absolute bottom-3 left-6 "
                />
              )}
            </div>
          ))}
          <div className=" relative">
            <p className="absolute cursor-pointer z-20 top-[14px] left-[20px] text-blanch ">
              {value?.slice(0, 4)}
            </p>
            <PhoneInput
              international
              ref={selectRef}
              defaultCountry="US"
              country={country}
              // value={value?.slice(4, value?.length)}
              value={value}
              onChange={(value) => {
                setValue(value as string);
              }}
            />
            <img
              src="/select.svg"
              alt=""
              className="absolute cursor-pointer top-[24px] left-[140px]"
            />
          </div>
          {formInput.slice(2, 4).map((input, index) => (
            <div className=" relative" key={index}>
              <Input
                placeholder={input.placeholder}
                type={input.type}
                design={"mt-[25px]"}
                register={register}
                errors={errors}
                required
                id={input.id}
                className={" rounded"}
              />
              {input.image && (
                <img
                  src={`${input.image}`}
                  draggable="false"
                  alt=""
                  className=" absolute bottom-3 left-6 "
                />
              )}
            </div>
          ))}
          <div className=" mt-7">
            <p className=" font-semibold pb-5">Je vous rejoins comme:</p>
            <div className=" flex flex-col gap-6">
              <div className=" flex gap-5 w-full">
                <div>
                  <div
                    className={` w-7 h-7  rounded-sm flex items-center justify-center cursor-pointer ${
                      checked1
                        ? "bg-primary"
                        : "border-secondary border-[1.5px]"
                    }`}
                    onClick={() => {
                      setChecked1(!checked1);
                    }}
                  >
                    {checked1 && <img src="/check.svg" alt="" />}
                  </div>
                </div>
                <p className=" text-grisp ">{"Actif"}</p>
              </div>
              <div className=" flex gap-5 w-full">
                <div>
                  <div
                    className={` w-7 h-7  rounded-sm flex items-center justify-center cursor-pointer ${
                      checked2
                        ? "bg-primary"
                        : "border-secondary border-[1.5px]"
                    }`}
                    onClick={() => {
                      setChecked2(!checked2);
                    }}
                  >
                    {checked2 && <img src="/check.svg" alt="" />}
                  </div>
                </div>
                <p className=" text-grisp ">{"Adhérent"}</p>
              </div>
              <div className=" flex gap-5 w-full">
                <div>
                  <div
                    className={` w-7 h-7  rounded-sm flex items-center justify-center cursor-pointer ${
                      checked3
                        ? "bg-primary"
                        : "border-secondary border-[1.5px]"
                    }`}
                    onClick={() => {
                      setChecked3(!checked3);
                    }}
                  >
                    {checked3 && <img src="/check.svg" alt="" />}
                  </div>
                </div>
                <p className=" text-grisp ">
                  {
                    "Je déclare souhaiter devenir membre de l'association : JAMII et accepte de verser ma cotisation annuelle de 5€."
                  }
                </p>
              </div>
            </div>
          </div>
          <div className=" mt-11">
            <CustomButton text="Soumettre" onClick={handleSubmit(onSubmit)} />
          </div>
        </Card>
      </div>
    </div>
  );
};

export default ModalContact;
