"use client";
import Image from "next/image";
import { usePathname, useRouter } from "next/navigation";
import React from "react";
import CustomButton from "./CustomButton";
interface HeaderProps {
  onClose: () => void;
}
const MiniHeader = ({ onClose }: HeaderProps) => {
  const pathname = usePathname();
  const links = [
    { name: "Accueil", link: "/", with: "w-[259px]" },
    { name: "Agenda", link: "/agenda", with: "w-[201px]" },
    { name: "Blog", link: "/blog", with: "w-[159px]" },
    { name: "Rejoins nous", link: "/join", with: "w-[113px]" },
    { name: "Contact", link: "/contact", with: "w-[100px]" },
  ];
  return (
    <div className=" fixed bg-black/70 top-0 bottom-0 left-0 right-0 z-20 ">
      <header className=" py-[35px] bg-[#000A21] h-screen justify-between w-full flex flex-col px-4">
        <div>
          <div className="flex justify-end">
            <Image
              className=" cursor-pointer"
              src={"/close.svg"}
              onClick={onClose}
              alt="logo"
              width={21}
              height={21}
            />
          </div>
          <div className=" flex w-full justify-center">
            <Image
              src={"/Jamii logo 1.svg"}
              alt="logo"
              width={66}
              height={61}
            />
          </div>
          <div className="flex flex-col gap-[62px] ">
            <nav className="flex flex-col gap-4">
              {links.map((link, index) => (
                <div key={index} className=" flex flex-col gap-4 ">
                  <a
                    className={`${
                      link.link === pathname
                        ? "text-primary"
                        : "text-blanch hover:text-primary"
                    } text-nowrap`}
                    href={`${link.link}`}
                  >
                    {" "}
                    {link.name}
                  </a>
                  <hr
                    className={`${
                      link.link === pathname ? "bg-primary" : " bg-grisp"
                    } rounded h-[2px] ${link.with}`}
                  />
                </div>
              ))}
            </nav>
            <CustomButton text="Faire un don" image="/Jamii gift.svg" />
          </div>
        </div>
        <div className=" flex flex-col h-full justify-between gap-[37px] pt-[37px]">
          <hr className=" bg-grisp rounded h-[2px]" />
          <div className=" flex flex-col gap-6">
            <CustomButton
              text="Rejoingnez-nous"
              image="/Join_ico.svg"
              color="text-[#F0FAFC]"
              className=" !bg-[#000A21]"
            />
            <CustomButton
              text="Contactez nous"
              image="/Phone_ico.svg"
              color="text-[#F0FAFC]"
              className=" !bg-[#00303F]"
            />
          </div>
        </div>
      </header>
    </div>
  );
};

export default MiniHeader;
