"use client";
import Image from "next/image";
import { usePathname } from "next/navigation";
import React from "react";

const Footer = () => {
  const pathname = usePathname();
  const ImagesData = [
    { image: "/mailfoot.svg", title: "info@jamii.com" },
    { image: "/tel.svg", title: "+32 471 52 32 20" },
    { image: "/location.svg", title: "Bruxelles, Belgique" },
  ];
  const Images = ["/TwitterIco.svg", "/FacebookIco.svg", "/LinkedinIco.svg"];
  const links = [
    { name: "Accueil", link: "/" },
    { name: "Agenda", link: "/agneda" },
    { name: "Rejoins nous", link: "/join" },
    { name: "Contact nous", link: "/contact" },
  ];
  return (
    <footer className="bg-noir relative overflow-hidden md:h-auto">
      <div className=" md:px-[180px] flex justify-between items-center relative py-16 text-blanch">
        <Image
          src={"/demilogo.svg"}
          alt="logo"
          width={307}
          height={400}
          className=" absolute left-0 top-0 hidden md:block"
        />
        <div className="flex items-center justify-center flex-col w-full">
          <div className=" flex items-center justify-center flex-col w-full">
            <div className=" flex flex-col gap-y-[35px] items-center">
              <Image
                className=" md:hidden"
                src={"/FooterLogo.svg"}
                alt="logo"
                width={95}
                height={87}
              />
              <nav className="flex flex-col gap-y-[30px] items-center md:gap-y-0 md:flex-row md:gap-x-[32px]">
                {links.map((link, index) => (
                  <div key={index}>
                    <a
                      className={`${
                        link.link === pathname
                          ? "text-primary"
                          : "text-blanch hover:text-primary"
                      } text-nowrap`}
                      href={`${link.link}`}
                    >
                      {" "}
                      {link.name}
                    </a>
                  </div>
                ))}
              </nav>
            </div>
            <div className="mt-7 px-4 w-full md:hidden">
              <hr className=" w-full border border-[#1D263D] h-[2px]" />
            </div>
            <div className=" flex flex-wrap gap-5 mt-7 items-center justify-center">
              {ImagesData.map((image, index) => (
                <div key={index} className="flex gap-x-3 items-center">
                  <div className="h-7 w-7 md:h-[45px] md:w-[45px] rounded-full flex items-center justify-center bg-white">
                    <Image
                      key={index}
                      src={`${image.image}`}
                      alt=""
                      width={24}
                      height={24}
                      className=" h-4 w-4 md:h-6 md:w-6"
                    />
                  </div>
                  <p className=" text-sm md:text-base">{image.title}</p>
                </div>
              ))}
            </div>
          </div>
          <div className="">
            <div className=" gap-2 mt-7 hidden md:flex">
              {Images.map((image, index) => (
                <div
                  key={index}
                  className="h-[24px] w-[24px] rounded-full flex items-center justify-center bg-[#96BCC5]"
                >
                  <Image src={`${image}`} alt="" width={11} height={11} />
                </div>
              ))}
            </div>
          </div>
        </div>
        <Image
          src={"/FooterLogo.svg"}
          alt="logo"
          width={224}
          height={208}
          className=" absolute right-[180px] hidden md:block"
        />
      </div>
      <div className="relative z-10  justify-center hidden md:flex py-[8px] bg-[#02091B]">
        <p className=" text-[#92989F] text-xs">Design with BxB design</p>
      </div>
    </footer>
  );
};

export default Footer;
