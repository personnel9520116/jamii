"use client";
import { CldUploadWidget } from "next-cloudinary";

// By default, the CldImage component applies auto-format and auto-quality to all delivery URLs for optimized delivery.
export default function Page() {
  return (
    <CldUploadWidget
      uploadPreset="upload-file"
      onSuccess={(results) => console.log({ results })}
    />
  );
}
