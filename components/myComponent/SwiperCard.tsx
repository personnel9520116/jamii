import React from "react";
import { Card, CardContent } from "../ui/card";

type SwiperCardProps = {
  infoData: {
    icon: string;
    image: string;
    title: string;
    description: string;
  };
};
const SwiperCard = ({ infoData }: SwiperCardProps) => {
  return (
    <Card className="bg-[#ffffff15] border-none flex items-center flex-col px-[32px] pt-[20px] h-[468px]  rounded-[15px]">
      <div className=" h-[140px] w-[140px] rounded-full relative flex items-center justify-center bg-noir">
        <img src={`${infoData.image}`} alt="" />
        <img
          src={`${infoData.icon}`}
          className=" absolute right-[-10px] top-[8px]"
          alt=""
        />
      </div>
      <CardContent className=" flex items-center flex-col pt-4">
        <h2 className=" text-grisf font-bold text-[20px]">{infoData.title}</h2>
        <p className=" text-gris pt-2">{infoData.description}</p>
      </CardContent>
    </Card>
  );
};

export default SwiperCard;
