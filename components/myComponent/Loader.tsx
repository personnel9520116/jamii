"use client";

export default function Loader() {
  return (
    <div className="flex h-full w-full items-center justify-center">
      <div className="flex flex-col items-center space-y-4">
        <div className="animate-spin rounded-full border-4 border-[#FFE3EE] border-t-[#FF67A0] h-12 w-12" />
        <p className="text-gray-500 dark:text-gray-400">Chargement...</p>
      </div>
    </div>
  );
}
