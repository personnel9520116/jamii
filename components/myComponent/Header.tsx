"use client";
import Image from "next/image";
import { usePathname, useRouter } from "next/navigation";
import React, { useState, useMemo } from "react";
import CustomButton from "./CustomButton";
import MiniHeader from "./MiniHeader";

const Header = () => {
  const pathname = usePathname();
  const [drawer, setDrawer] = useState<boolean>(false);

  const links = useMemo(
    () => [
      { name: "Accueil", link: "/" },
      { name: "Agenda", link: "/agenda" },
      { name: "Blog", link: "/blog" },
      { name: "Rejoins nous", link: "/join" },
      { name: "Contact", link: "/contact" },
    ],
    []
  );

  const isActive = (link: string) => {
    if (link === "/") {
      return pathname === link;
    }
    return pathname.startsWith(link);
  };

  return (
    <header className=" py-[20px] bg-[#000A21] w-full flex items-center justify-between px-4 lg:px-[90px]">
      <Image src={"/Jamii logo 1.svg"} alt="logo" width={66} height={61} />
      <div className=" hidden lg:flex gap-x-8 items-center">
        <nav className="flex gap-x-8">
          {links.map((link, index) => (
            <div key={index} className=" flex flex-col ">
              <a
                className={`${
                  isActive(link.link)
                    ? "text-primary"
                    : "text-blanch hover:text-primary"
                } text-nowrap`}
                href={`${link.link}`}
              >
                {" "}
                {link.name}
              </a>
              {isActive(link.link) && (
                <div className=" flex mt-1">
                  <hr className=" bg-primary rounded w-[33px] h-[3px]" />
                  <hr className=" bg-primary rounded ml-[2px] w-[3px] h-[3px]" />
                </div>
              )}
            </div>
          ))}
        </nav>
        <CustomButton text="Faire un don" image="/Jamii gift.svg" />
      </div>
      <Image
        src={"/hamburger.svg"}
        alt="logo"
        className=" lg:hidden cursor-pointer"
        width={30}
        height={20}
        onClick={() => setDrawer(true)}
      />
      {drawer && <MiniHeader onClose={() => setDrawer(false)} />}
    </header>
  );
};

export default Header;
