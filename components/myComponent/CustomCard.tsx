import React from "react";
import { CardContent, Card, CardFooter } from "../ui/card";
import Image from "next/image";
import CustomButton from "./CustomButton";
import Link from "next/link";
type CustomCardProps = {
  bgColor?: string;
  ishome?: boolean;
  cardData: {
    image: string;
    title: string;
    subTitle: string;
  };
};
const CustomCard = ({ bgColor, cardData, ishome }: CustomCardProps) => {
  return (
    <Link href={"/agenda/id"} className=" w-full">
      <Card
        className={`group w-full  border-none ${
          bgColor ? bgColor : "bg-secondary"
        }  ${
          bgColor ? " text-noir" : "text-blanch"
        } rounded-[13px] cursor-pointer ${
          ishome ? " min-h-[424px] hover:h-[488px] " : ""
        }`}
      >
        <div
          className={` ${
            ishome ? "" : "h-[144px] lg:h-[260px]"
          }  rounded-t-[13px] overflow-hidden`}
        >
          <img
            src={`${cardData?.image ?? ""}`}
            alt=""
            style={{
              width: "100%",
              height: "100%",
              objectFit: "cover",
              aspectRatio: "16/9",
            }}
          />
        </div>
        <CardContent
          className={` flex ${
            ishome ? " items-start" : " items-center"
          } flex-col pt-3 pb-6 md:p-8 md:pt-4`}
        >
          <h2
            className={`${
              ishome ? "text-[20px]" : "text-sm md:text-[20px] text-center"
            } font-bold `}
          >
            {cardData.title ?? ""}
          </h2>
          <p
            className={` ${
              ishome ? "text-base" : "text-sm md:text-base text-center "
            }${bgColor ? " text-noir" : "text-gris"} pt-1 md:pt-2`}
          >
            {cardData.subTitle ?? ""}
          </p>
        </CardContent>
        {ishome && (
          <CardFooter className="md:hidden md:group-hover:flex justify-center read">
            <CustomButton
              text="Lire"
              className="bg-transparent border-[1.5px] border-secondary w-[185px]"
            />
          </CardFooter>
        )}
      </Card>
    </Link>
  );
};

export default CustomCard;
