"use client";

import Image from "next/image";
import React from "react";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import { Popover, PopoverContent, PopoverTrigger } from "../ui/popover";
import { signOut } from "next-auth/react";
import { useUserStore } from "@/store/stores";

const AdminHeader = () => {
  const { user } = useUserStore();

  return (
    <header className="py-[20px] bg-[#000A21] w-full flex items-center justify-end px-4 lg:px-[90px]">
      <div className="flex gap-3 items-center">
        <p className="text-grisf mr-[22px]">Lorem ipsum</p>
        <Popover>
          <PopoverTrigger asChild>
            <div className="bg-secondary cursor-pointer rounded-[10px] items-center flex gap-2 px-4 py-2">
              <p className="text-grisf">{user.name || "Utilisateur"}</p>
              <Avatar className="w-[44px] h-[44px]">
                <AvatarImage
                  src={user.image || "/Jamii President.png"}
                  alt="Avatar utilisateur"
                />
                <AvatarFallback>{user.name?.[0] || "U"}</AvatarFallback>
              </Avatar>
              <Image src="/select.svg" alt="logo" width={10} height={6} />
            </div>
          </PopoverTrigger>
          <PopoverContent className="p-5 bg-secondary flex gap-3 flex-col border-none text-grisf w-auto">
            <p className="cursor-pointer">Profil</p>
            <p className="cursor-pointer" onClick={() => signOut()}>
              Se déconnecter
            </p>
          </PopoverContent>
        </Popover>
        <Popover>
          <PopoverTrigger asChild>
            <div className="bg-secondary cursor-pointer rounded-[10px] items-center flex gap-2 px-2 py-4">
              <Image src="/flag.svg" alt="logo" width={29} height={29} />
              <Image src="/select.svg" alt="logo" width={10} height={6} />
            </div>
          </PopoverTrigger>
          <PopoverContent className="p-5 bg-secondary border-none flex gap-3 flex-col text-grisf w-auto">
            <div className="flex gap-2 items-center cursor-pointer">
              <Image src="/flag.svg" alt="logo" width={29} height={29} />
              <p>Français</p>
            </div>
            <div className="flex gap-2 items-center cursor-pointer">
              <Image src="/flag.svg" alt="logo" width={29} height={29} />
              <p>Anglais</p>
            </div>
          </PopoverContent>
        </Popover>
      </div>
    </header>
  );
};

export default AdminHeader;
