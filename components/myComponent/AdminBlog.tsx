"use client";
import React from "react";
import { Card, CardContent, CardFooter } from "../ui/card";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import { useParams, usePathname, useRouter } from "next/navigation";
import Image from "next/image";
import { toast } from "react-toastify";

type Props = {
  disabled?: boolean;
  onAgenda?: boolean;
  onBlog?: boolean;
  isSame?: boolean;
  blogData?: {
    id?: string;
    category?: string;
    title: string;
    description: string;
    image: string;
    auteur?: {
      avatar: string;
      nom: string;
    };
    createAt?: Date | string;
    partage?: boolean;
  };
};

const AdminBlog = ({ disabled, onAgenda, onBlog, blogData, isSame }: Props) => {
  const router = useRouter();
  const handleDelete = async (id: string) => {
    try {
      const response = await fetch(`/api/post/${id}`, {
        method: "DELETE",
      });
      if (response.ok) {
        setTimeout(() => {
          window.location.href = "/admin/blog";
        }, 2000);
        toast.success("Article supprimé avec succès");
      } else {
        toast.error("Erreur lors de la suppression de l'article");
      }
    } catch (error) {
      toast.error("Erreur lors de la suppression de l'article");
    }
  };
  return (
    <Card
      className={`w-full p-2 md:p-5 rounded-[13px] h-[354px] border-[1.5px] text-blanch cursor-pointer`}
    >
      <div className={`h-[195px] rounded-[10px] overflow-hidden `}>
        <img
          src={`${blogData?.image ?? ""}`}
          alt=""
          style={{
            width: "100%",
            height: "100%",
            objectFit: "cover",
            aspectRatio: "16/9",
          }}
        />
      </div>
      <CardContent className=" px-0 pb-4  pt-[10px]">
        <h2 className=" text-secondary font-semibold ">
          {blogData?.title ?? ""}
        </h2>
        <p className={`text-sm text-grisp pt-2 h-[39px]`}>
          {blogData?.description ?? ""}
        </p>
      </CardContent>
      <div
        className={` flex items-center ${
          disabled ? "justify-end" : "justify-between"
        } `}
      >
        {disabled ? null : (
          <div>
            {onBlog ? (
              <div
                className={`${
                  onBlog
                    ? " flex gap-x-2 items-center "
                    : "flex flex-col md:flex-row md:items-center gap-x-2"
                } text-xs text-grisp`}
              >
                <div className=" flex gap-x-2  items-center">
                  <Avatar
                    className={`${
                      onBlog ? "h-7 w-7" : " h-3 w-3 md:h-7 md:w-7"
                    }`}
                  >
                    <AvatarImage
                      src={`${blogData?.auteur?.avatar ?? ""}`}
                      alt="test"
                    />
                    <AvatarFallback>cn</AvatarFallback>
                  </Avatar>
                  <p>{blogData?.auteur?.nom ?? ""}</p>
                </div>
              </div>
            ) : (
              <button className=" bg-[#0C8B28] px-[40px] py-[6px]">
                Désarchiver
              </button>
            )}
          </div>
        )}
        <div className=" flex gap-x-4">
          <button onClick={() => handleDelete(blogData?.id ?? "")}>
            <Image src="/delete.svg" alt="" width={35} height={35} />
          </button>
          <button
            onClick={() => router.push(`/admin/blog/edit/${blogData?.id}`)}
          >
            <Image src="/edit.svg" alt="" width={35} height={35} />
          </button>
        </div>
      </div>
    </Card>
  );
};

export default AdminBlog;
