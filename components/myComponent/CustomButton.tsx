import React from "react";

type Props = {
  color?: string;
  text: string;
  image?: string;
  height?: string;
  width?: string;
  bgcolor?: string;
  className?: string;
  onClick?: () => void;
};
const CustomButton = ({
  color,
  text,
  image,
  height,
  width,
  bgcolor,
  onClick,
  className,
}: Props) => {
  return (
    <button
      className={`${
        height ? `h-[${height}]` : "h-[52px]"
      }  flex items-center gap-x-[10px] px-5 justify-center ${
        width ? `md:w-[${width}]` : "w-full"
      } rounded-[25px] ${
        bgcolor ? `bg-[${bgcolor}]` : "bg-primary"
      } ${className}`}
      onClick={onClick}
    >
      <img src={`${image ?? ""}`} alt="" />
      <p className={`${color ? `${color}` : "text-noir"}`}>{text}</p>
    </button>
  );
};

export default CustomButton;
