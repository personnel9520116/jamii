"use client";
import Image from "next/image";
import { usePathname } from "next/navigation";
import React from "react";

const Sidebar = () => {
  const pathname = usePathname();
  const sideItem = [
    {
      icon: "/dashboard.svg",
      title: "Tableau de bord",
      link: "/admin",
    },
    { icon: "/blog.svg", title: "Mon blog", link: "/admin/blog" },
    {
      icon: "/Projet_sidebar.svg",
      title: "Mes projets",
      link: "/admin/projet",
    },
    {
      icon: "/archive.svg",
      title: "Mes archives",
      link: "/admin/archive",
    },
  ];
  const isActive = (link: string) => {
    if (link === "/admin") {
      return pathname === link;
    }
    return pathname.startsWith(link);
  };
  return (
    <div className=" relative bg-[#000A21] flex flex-col items-center pt-5 pl-[35px] pr-[18px] h-screen overflow-scroll w-[292px] ">
      <Image src={"/Jamii logo 1.svg"} alt="logo" width={78} height={73} />
      <div className=" flex flex-col gap-5 mt-[48px] w-full">
        {sideItem.map((item, index) => (
          <a
            key={index}
            className={`${
              isActive(item.link) ? " bg-primary" : " bg-[#030E27] "
            } py-4 px-5 flex gap-[15px] rounded-[10px]`}
            href={item.link}
          >
            <div>
              <Image src={item.icon} alt="logo" width={20} height={22} />
            </div>
            <p
              className={`${
                isActive(item.link)
                  ? " bg-primary text-noir"
                  : " bg-[#030E27] text-[#5C7F87]"
              }`}
            >
              {item.title}
            </p>
          </a>
        ))}
        <a
          className="bg-[#030E27] w-[238px]  absolute bottom-[58px] text-[#5C7F87] py-4 px-5 flex gap-[15px] rounded-[10px]"
          href={""}
        >
          <Image src="/profil.svg" alt="logo" width={20} height={22} />
          <p>Profil</p>
        </a>
      </div>
    </div>
  );
};

export default Sidebar;
