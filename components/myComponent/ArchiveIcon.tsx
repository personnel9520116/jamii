import React from "react";
interface IconProps {
  className?: string;
}
const ArchiveIcon = ({ className }: IconProps) => {
  return (
    <svg
      className={className}
      width="21"
      height="20"
      viewBox="0 0 21 20"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 3.66629C1 3.10639 1.22242 2.56942 1.61833 2.17351C2.01424 1.7776 2.55121 1.55518 3.11111 1.55518H17.8889C18.4488 1.55518 18.9858 1.7776 19.3817 2.17351C19.7776 2.56942 20 3.10639 20 3.66629C20 4.22619 19.7776 4.76316 19.3817 5.15907C18.9858 5.55498 18.4488 5.7774 17.8889 5.7774H3.11111C2.55121 5.7774 2.01424 5.55498 1.61833 5.15907C1.22242 4.76316 1 4.22619 1 3.66629Z"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M3.11084 5.77686V16.3324C3.11084 16.8923 3.33326 17.4293 3.72917 17.8252C4.12508 18.2211 4.66205 18.4435 5.22195 18.4435H15.7775C16.3374 18.4435 16.8744 18.2211 17.2703 17.8252C17.6662 17.4293 17.8886 16.8923 17.8886 16.3324V5.77686"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M8.38916 10H12.6114"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};

export default ArchiveIcon;
