"use client";
import React from "react";
import { Card, CardContent } from "../ui/card";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import Image from "next/image";
import Link from "next/link";
import { format } from "date-fns";
import { Post } from "@/types/type";

type Props = {
  disabled?: boolean;
  onAgenda?: boolean;
  onBolg?: boolean;
  isSame?: boolean;
  blogData?: Post;
};

const CardBlog = ({ disabled, onAgenda, onBolg, blogData, isSame }: Props) => {
  return (
    <Link href={`/blog/${blogData?.id}`}>
      <Card
        className={`${
          onBolg ? "min-h-[484px]" : " "
        } w-full p-2 md:p-5 rounded-[13px] border-none text-blanch cursor-pointer`}
      >
        <div
          className={`${
            onBolg ? "h-[203px]" : "h-[124px] lg:h-[203px]"
          }  rounded-t-[10px] overflow-hidden `}
        >
          <img
            src={`${blogData?.image ?? ""}`}
            alt=""
            style={{
              width: "100%",
              height: "100%",
              objectFit: "cover",
              aspectRatio: "16/9",
            }}
          />
        </div>
        <CardContent className=" px-0 pb-2 md:pb-5 pt-1 md:pt-4">
          <p className={`${onBolg ? "text-grisp  py-2" : "hidden"}`}>
            {blogData?.category ?? ""}
          </p>
          <h2 className=" text-secondary font-bold text-sm md:text-[20px] h-[56px]">
            {blogData?.title ?? ""}
          </h2>
          <p
            className={`${
              onBolg || isSame ? " block min-h-[96px]" : " hidden md:block "
            } text-sm md:text-base text-grisp pt-2`}
          >
            {blogData?.description ?? ""}
          </p>
        </CardContent>
        {disabled ? null : (
          <div className={" flex items-center justify-between"}>
            <div
              className={`${
                onBolg
                  ? " flex gap-x-2 items-center "
                  : "flex flex-col md:flex-row md:items-center gap-x-2"
              } text-xs text-grisp`}
            >
              <div className=" flex gap-x-2  items-center">
                <Avatar
                  className={`${onBolg ? "h-7 w-7" : " h-3 w-3 md:h-7 md:w-7"}`}
                >
                  <AvatarImage
                    src={`${blogData?.author?.image ?? ""}`}
                    alt="test"
                  />
                  <AvatarFallback>cn</AvatarFallback>
                </Avatar>
                <p>{blogData?.author?.name ?? ""}</p>
                <p className=" hidden md:block">-</p>
              </div>
              <p>
                {format(blogData?.createdAt ?? new Date(), "MM/dd/yyyy") ?? ""}
              </p>
            </div>
            {blogData?.share && (
              <button>
                <Image src="/share.svg" alt="" width={41} height={41} />
              </button>
            )}
          </div>
        )}
      </Card>
    </Link>
  );
};

export default CardBlog;
