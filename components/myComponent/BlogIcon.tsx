import React from "react";
interface IconProps {
  className?: string;
}
const BlogIcon = ({ className }: IconProps) => {
  return (
    <svg
      className={className}
      width="21"
      height="25"
      viewBox="0 0 21 25"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 3.85C1 3.09413 1.22242 2.36922 1.61833 1.83475C2.01424 1.30027 2.55121 1 3.11111 1H17.8889C18.4488 1 18.9858 1.30027 19.3817 1.83475C19.7776 2.36922 20 3.09413 20 3.85V20.95C20 21.7059 19.7776 22.4308 19.3817 22.9653C18.9858 23.4997 18.4488 23.8 17.8889 23.8H3.11111C2.55121 23.8 2.01424 23.4997 1.61833 22.9653C1.22242 22.4308 1 21.7059 1 20.95V3.85Z"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M4.7998 7.08008H15.4398"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M4.7998 12.3999H15.4398"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M4.7998 17.7202H15.4398"
        stroke="#5C7F87"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};

export default BlogIcon;
