import React from "react";
import { Card, CardContent, CardTitle } from "../ui/card";
import Image from "next/image";

type Props = {
  image?: string;
  name?: string;
  role?: string;
  description?: string;
};
const CardOrganism = ({ image, name, role, description }: Props) => {
  return (
    <Card
      className={`${
        description
          ? "p-[20px] gap-x-[34px] items-center flex-row"
          : " flex-col "
      } bg-grisf flex rounded-[13px] text-secondary`}
    >
      <Image
        src={"/Jamii President.png"}
        alt="number"
        width={209}
        height={205}
      />
      <div>
        <CardContent className={`${description ? "px-0" : "px-5"}`}>
          <CardTitle
            className={` ${
              description ? "text-2xl" : "text-lg pt-4"
            } font-bold  text-noir`}
          >
            Oufad Ndezip
          </CardTitle>
          <p className="text-secondary text-sm pt-[5px]">Vice présidente</p>
          {description ? (
            <p className=" text-grisp text-sm pt-[5px]">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam
            </p>
          ) : null}
        </CardContent>
      </div>
    </Card>
  );
};

export default CardOrganism;
