"use client";

import { useEffect } from "react";
import { useUserStore } from "@/store/stores";

type Props = {
  user: any; // Définissez le type approprié pour votre utilisateur
  children: React.ReactNode;
};

export function ClientWrapper({ user, children }: Props) {
  const setUser = useUserStore((state) => state.setUser);

  useEffect(() => {
    if (user) {
      setUser(user);
    }
  }, [user, setUser]);

  return <>{children}</>;
}
